// WEB BUILD Entry point
// Used for both 'dev' mode and 'production' mode builds
import React from 'react'
import { createRoot } from 'react-dom/client'

// APPLICATION ROOT COMPONENT
import App from './app'

// REACT ROUTER DOM
import { BrowserRouter } from 'react-router-dom'

// CONTEXT
import ViewportContextProvider from './context/viewportContextProvider'
import ThemeContextProvider from './context/ThemeContextProvider'
// RENDER ROOT COMPONENT
const container = document.getElementById('application')
const root = createRoot(container)
root.render(
  <BrowserRouter>
    <ViewportContextProvider>
      <ThemeContextProvider>
        <App />
      </ThemeContextProvider>
    </ViewportContextProvider>
  </BrowserRouter>
)
