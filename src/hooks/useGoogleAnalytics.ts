import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import ReactGA from "react-ga4";

const useGoogleAnalytics = ( measurementId : string ) => {
  const location = useLocation();

  useEffect(() => {
    ReactGA.initialize(measurementId);
  },[]);

  useEffect(() => {
    ReactGA.send({ hitType: 'pageview', page: location.pathname })
  }, [location]);
};

export default useGoogleAnalytics;