import React, { useEffect } from 'react'
import classNames from 'classnames/bind'
import { useLocation } from 'react-router-dom'
import propTypes from 'prop-types'

import SideNav from '../sideNav/sideNav'
import ThemeSelector from '../common/themeSelector/themeSelector'

import { THEME } from '../../constants/theme'
import styles from './rootView.module'
import { ThemeNamesListType } from '../../Types'
let cx = classNames.bind(styles)

type RootViewPropsType = {
  children: JSX.Element[] | JSX.Element,
  themeName: ThemeNamesListType,
  hasRandomTheme: boolean,
  setHasRandomTheme: Function
}

const RootView = ({ children, themeName, hasRandomTheme, setHasRandomTheme }: RootViewPropsType) : JSX.Element => {
  const location = useLocation()
  useEffect(() => {
    if (location.hash === '') {
      window.scrollTo({ top: 0 })
    }
  }, [location])

  const closeToast = () => {
    setHasRandomTheme(false)
  }

  let containerClasses = cx({
    appContainer: true,
    theme_haas: themeName === THEME.HAAS,
    theme_sprawl: themeName === THEME.SPRAWL,
  })

  return (
    <article className={containerClasses}>
      <SideNav />
      <main className={styles.contentContainer}>
        {children}
        {hasRandomTheme && (
          <div className={styles.themeToast}>
            <p>Hi,</p>
            <p>
              As its your first time here we&apos;ve randomly selected the{' '}
              <strong>&quot;{themeName}&quot;</strong> theme for you.
            </p>
            <p>
              Please take a look at the range of themes available in the Settings area
              (bottom-right). Thanks for visiting, I hope you enjoy my site.
            </p>
            <p> - Anthony Cregan</p>
            <button className={styles.dismissButton} onClick={() => closeToast()}>
              &times;
            </button>
          </div>
        )}
        <ThemeSelector />
      </main>
    </article>
  )
}

RootView.propTypes = {
  children: propTypes.node.isRequired,
  themeName: propTypes.string.isRequired,
  hasRandomTheme: propTypes.bool.isRequired,
  setHasRandomTheme: propTypes.func.isRequired,
}

export default RootView
