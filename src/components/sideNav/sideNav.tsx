import React, { useState, useEffect, useRef, useContext } from 'react'
import classNames from 'classnames/bind'
import { useScreenLargerThan } from '../../utilities/utilities'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import SvgIcon from '../common/svgIcon/svgIcon'
import ArtDecoSideNav from './artDecoSideNav/artDecoSideNav'
import OriginalSideNav from './originalSideNav/originalSideNav'
import SprawlSideNav from './sprawlSideNav/sprawlSideNav'
import { ThemeContext } from '../../context/ThemeContextProvider'
import { THEME } from '../../constants/theme'
import SantaCruzSideNav from './santaCruzSideNav/santaCruzSideNav'
import HaasSideNav from './haasSideNav/haasSideNav'

import { ThemeNamesListType } from '../../Types'

import styles from './sideNav.module'
let cx = classNames.bind(styles)

const SideNav = () : JSX.Element => {
  const [menuOpen, setMenuOpen] = useState(false)
  const screenLargerThanSm = useScreenLargerThan('sm')
  const { themeName } = useContext(ThemeContext)

  // Set menu to closed if screen is resized larger than SM
  useEffect(() => {
    if (screenLargerThanSm && menuOpen) {
      setMenuOpen(false)
    }
  }, [screenLargerThanSm, menuOpen])

  let menuClasses = cx({
    sideNavContainer: true,
    sideNavContainer_open: menuOpen === true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_sprawl: themeName === THEME.SPRAWL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_haas: themeName === THEME.HAAS,
  })

  // close on click outside menu when in tablet/mobile mode
  const navRef = useRef()
  useOnClickOutside(navRef, () => {
    if (!screenLargerThanSm && menuOpen) {
      setMenuOpen(false)
    }
  })

  const renderMenu = (theme: ThemeNamesListType) => {
    switch (theme) {
      case THEME.SANTACRUZ:
        return <SantaCruzSideNav />
      case THEME.ORIGINAL:
        return <OriginalSideNav />
      case THEME.ARTDECO:
        return <ArtDecoSideNav />
      case THEME.SPRAWL:
        return <SprawlSideNav />
      case THEME.HAAS:
        return <HaasSideNav />
      default:
        return 'Create a Menu'
    }
  }

  return (
    <nav className={menuClasses} ref={navRef}>
      <div className={styles.fixedMenuContainer}>{renderMenu(themeName)}</div>

      <div
        className={styles.menuToggle}
        onClick={() => {
          setMenuOpen(!menuOpen)
        }}
      >
        <SvgIcon image="Burger" />
      </div>
    </nav>
  )
}

export default SideNav
