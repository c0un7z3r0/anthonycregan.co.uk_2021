import React, { useContext } from 'react'
import propTypes from 'prop-types'
import { ThemeContext } from '../../../context/ThemeContextProvider'
import { THEME } from '../../../constants/theme'
import CommonNavMenuItem from './commonNavMenuItem/commonNavMenuItem'
import classNames from 'classnames/bind'
let cx = classNames.bind(styles)
import styles from './commonNavMenu.module'
import { NavMenuItemType } from '../../../Types'

type CommonNavMenuPropsType = {
  menuConfig: NavMenuItemType[]
}

const CommonNavMenu = ({ menuConfig }: CommonNavMenuPropsType) : JSX.Element => {
  const { themeName } = useContext(ThemeContext)

  let menuListClasses = cx({
    menuList: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_sprawl: themeName === THEME.SPRAWL,
    theme_haas: themeName === THEME.HAAS,
  })

  return (
    <ul className={menuListClasses}>
      {menuConfig.map((item) => (
        <CommonNavMenuItem
          key={item.label}
          themeName={themeName}
          label={item.label}
          path={item.path}
          icon={item.icon}
          subMenu={item.subMenu}
        />
      ))}
    </ul>
  )
}

CommonNavMenu.propTypes = {
  menuConfig: propTypes.array.isRequired,
}

export default CommonNavMenu
