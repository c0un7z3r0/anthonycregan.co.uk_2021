import React, { useRef, useEffect, useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import propTypes from 'prop-types'
import classNames from 'classnames/bind'
import { ThemeContext } from '../../../../../context/ThemeContextProvider'
import { THEME } from '../../../../../constants/theme'

import { SubMenuItemPropsType } from '../../../../../Types'

import styles from './subMenu.module.scss'
let cx = classNames.bind(styles)

type SubMenuPropsType = {
  linkArray: SubMenuItemPropsType[],
  path: string, 
  currentPath: string, 
  currentHash: string
}

const SubMenu = ({ linkArray, path, currentPath, currentHash }:SubMenuPropsType): JSX.Element => {
  const subMenuListRef = useRef()
  const { themeName } = useContext(ThemeContext)
  const [menuOpen, setMenuOpen] = useState(false)

  useEffect(() => {
    if (path === currentPath) {
      setMenuOpen(true)
    } else {
      setMenuOpen(false)
    }
  }, [path, currentPath])

  // Menu classes
  let subMenuClasses = cx({
    theme_artDeco_subMenu_open: themeName === THEME.ARTDECO && menuOpen === true,
    theme_artDeco_subMenu_closed: themeName === THEME.ARTDECO && menuOpen === false,
    theme_original_subMenu_open: themeName === THEME.ORIGINAL && menuOpen === true,
    theme_original_subMenu_closed: themeName === THEME.ORIGINAL && menuOpen === false,
    theme_santaCruz_subMenu_open: themeName === THEME.SANTACRUZ && menuOpen === true,
    theme_santaCruz_subMenu_closed: themeName === THEME.SANTACRUZ && menuOpen === false,
    theme_sprawl_subMenu_open: themeName === THEME.SPRAWL && menuOpen === true,
    theme_sprawl_subMenu_closed: themeName === THEME.SPRAWL && menuOpen === false,
    theme_haas_subMenu_open: themeName === THEME.HAAS && menuOpen === true,
    theme_haas_subMenu_closed: themeName === THEME.HAAS && menuOpen === false,
  })
  // Menu Item classes
  let subMenuListItemClasses = (menuItemHash: string) =>
    cx({
      theme_artDeco_subMenuItem_closed: themeName === THEME.ARTDECO && menuOpen === false,
      theme_artDeco_subMenuItem_open: themeName === THEME.ARTDECO && menuOpen === true,
      theme_artDeco_subMenuItemActive:
        themeName === THEME.ARTDECO && currentHash === `${menuItemHash}`,
      theme_original_subMenuItem_closed: themeName === THEME.ORIGINAL && menuOpen === false,
      theme_original_subMenuItem_open: themeName === THEME.ORIGINAL && menuOpen === true,
      theme_original_subMenuItemActive:
        themeName === THEME.ORIGINAL && currentHash === `${menuItemHash}`,
      theme_santaCruz_subMenuItem_closed: themeName === THEME.SANTACRUZ && menuOpen === false,
      theme_santaCruz_subMenuItem_open: themeName === THEME.SANTACRUZ && menuOpen === true,
      theme_santaCruz_subMenuItemActive:
        themeName === THEME.SANTACRUZ && currentHash === `${menuItemHash}`,
      theme_sprawl_subMenuItem_closed: themeName === THEME.SPRAWL && menuOpen === false,
      theme_sprawl_subMenuItem_open: themeName === THEME.SPRAWL && menuOpen === true,
      theme_sprawl_subMenuItemActive:
        themeName === THEME.SPRAWL && currentHash === `${menuItemHash}`,
      theme_haas_subMenuItem_closed: themeName === THEME.HAAS && menuOpen === false,
      theme_haas_subMenuItem_open: themeName === THEME.HAAS && menuOpen === true,
      theme_haas_subMenuItemActive: themeName === THEME.HAAS && currentHash === `${menuItemHash}`,
    })

  return (
    <ul className={subMenuClasses} ref={subMenuListRef}>
      {linkArray.map((menuItem, i) => {
        const transitionDelayStyle = {
          transitionDelay: `${100 * i}ms`,
        }
        return (
          <li
            className={subMenuListItemClasses(menuItem.hash)}
            key={`${menuItem.hash}_${i}`}
            style={themeName === THEME.ARTDECO ? transitionDelayStyle : null}
          >
            <Link to={`${currentPath}${menuItem.hash}`}>{menuItem.label}</Link>
          </li>
        )
      })}
    </ul>
  )
}

SubMenu.propTypes = {
  linkArray: propTypes.array.isRequired,
  path: propTypes.string.isRequired,
  currentPath: propTypes.string.isRequired,
  currentHash: propTypes.string,
}

export default SubMenu
