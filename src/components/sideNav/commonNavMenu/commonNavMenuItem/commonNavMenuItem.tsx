import React from 'react'
import propTypes from 'prop-types'
import { Link, useLocation } from 'react-router-dom'
import SvgIcon from '../../../common/svgIcon/svgIcon'
import SubMenu from './subMenu/subMenu'
import classNames from 'classnames/bind'
import styles from './commonNavMenuItem.module'
import { THEME } from '../../../../constants/theme'
import { NavMenuItemType } from '../../../../Types'

let cx = classNames.bind(styles)

type CommonNavMenuItemPropsType = NavMenuItemType

const CommonNavMenuItem = ({ themeName, label, path, icon, subMenu }: CommonNavMenuItemPropsType) : JSX.Element => {
  const location = useLocation()

  const menuItemClasses = (link: string) =>
    cx({
      artdeco_menuItem: themeName === THEME.ARTDECO,
      artdeco_menuItem_active: location.pathname === link && themeName === THEME.ARTDECO,
      original_menuItem: themeName === THEME.ORIGINAL,
      original_menuItem_active: location.pathname === link && themeName === THEME.ORIGINAL,
      santaCruz_menuItem: themeName === THEME.SANTACRUZ,
      santaCruz_menuItem_active: location.pathname === link && themeName === THEME.SANTACRUZ,
      sprawl_menuItem: themeName === THEME.SPRAWL,
      sprawl_menuItem_active: location.pathname === link && themeName === THEME.SPRAWL,
      haas_menuItem: themeName === THEME.HAAS,
      haas_menuItem_active: location.pathname === link && themeName === THEME.HAAS,
    })

  return (
    <>
      <li className={menuItemClasses(path)}>
        <Link to={path}>
          {icon && (
            <div className={styles.iconContainer}>
              <SvgIcon image={icon} />
            </div>
          )}
          <span className={styles.label}>{label}</span>
        </Link>
      </li>
      {subMenu && (
        <SubMenu
          linkArray={subMenu}
          path={path}
          currentPath={location.pathname}
          currentHash={location.hash}
        />
      )}
    </>
  )
}

CommonNavMenuItem.propTypes = {
  themeName: propTypes.string.isRequired,
  label: propTypes.string.isRequired,
  path: propTypes.string.isRequired,
  icon: propTypes.string,
  subMenu: propTypes.array,
}

export default CommonNavMenuItem
