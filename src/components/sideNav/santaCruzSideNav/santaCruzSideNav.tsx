import React from 'react'
import CommonNavMenu from '../commonNavMenu/commonNavMenu'
import SvgIcon from '../../common/svgIcon/svgIcon'
import styles from './santaCruzSideNav.module'

const SantaCruzSideNav = () : JSX.Element => {
  const menuConfig = [
    {
      label: 'Home',
      path: '/',
      icon: 'HomeIcon',
    },
    {
      label: 'Portfolio',
      path: '/portfolio',
      icon: 'PortfolioIcon',
      subMenu: [
        { label: 'Aleph-Labs', hash: '#aleph' },
        { label: 'UOB Sustainability', hash: '#uob-sustainability' },
        { label: 'Standard Chartered Bank', hash: '#standard-chartered' },
        { label: 'UOB', hash: '#uob' },
        { label: 'Booking.com', hash: '#booking' },
        { label: 'AE Live', hash: '#aelive' },
        { label: 'Betfred', hash: '#betfred' },
        { label: 'Perpetual Summer', hash: '#perpetual-summer' },
        { label: 'SIS', hash: '#sis' },
        { label: 'Archive', hash: '#archive' },
      ],
    },
    {
      label: 'Projects',
      path: '/projects',
      icon: 'ProjectIcon',
      subMenu: [
        { label: 'Scribe CMS', hash: '#scribe-cms' },
        { label: 'Perpetual Summer Ltd', hash: '#perpetual-summer' },
        { label: 'Catalyst Elements', hash: '#catalyst-elements' },
        { label: 'React Catalyst', hash: '#react-catalyst' },
        { label: 'Cryptotron', hash: '#cryptotron' },
        { label: 'Touchscreen Tablet', hash: '#touchscreen' },
      ],
    },
    {
      label: 'Resume',
      path: '/resume',
      icon: 'CvIcon',
      subMenu: [
        { label: 'Page 1', hash: '#page1' },
        { label: 'Page 2', hash: '#page2' },
        { label: 'Page 3', hash: '#page3' },
        { label: 'Page 4', hash: '#page4' },
      ],
    },
    {
      label: 'Contact',
      path: '/contact',
      icon: 'ContactIcon',
    },
  ]

  return (
    <div className={styles.container}>
      <SvgIcon image="SantaCruz" />
      <CommonNavMenu menuConfig={menuConfig} />
    </div>
  )
}

export default SantaCruzSideNav
