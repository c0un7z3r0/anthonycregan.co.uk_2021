import React from 'react'
import CommonNavMenu from '../commonNavMenu/commonNavMenu'
import styles from './haasSideNav.module.scss'

const HaasSideNav = (): JSX.Element => {
  const menuConfig = [
    {
      label: 'Home',
      path: '/',
    },
    {
      label: 'Portfolio',
      path: '/portfolio',
      subMenu: [
        { label: 'Aleph-Labs', hash: '#aleph' },
        { label: 'UOB Sustainability', hash: '#uob-sustainability' },
        { label: 'Standard Chartered Bank', hash: '#standard-chartered' },
        { label: 'UOB', hash: '#uob' },
        { label: 'Booking.com', hash: '#booking' },
        { label: 'AE Live', hash: '#aelive' },
        { label: 'Betfred', hash: '#betfred' },
        { label: 'Perpetual Summer', hash: '#perpetual-summer' },
        { label: 'SIS', hash: '#sis' },
        { label: 'Archive', hash: '#archive' },
      ],
    },
    {
      label: 'Projects',
      path: '/projects',
      subMenu: [
        { label: 'Scribe CMS', hash: '#scribe-cms' },
        { label: 'Perpetual Summer Ltd', hash: '#perpetual-summer' },
        { label: 'Catalyst Elements', hash: '#catalyst-elements' },
        { label: 'React Catalyst', hash: '#react-catalyst' },
        { label: 'Cryptotron', hash: '#cryptotron' },
        { label: 'Touchscreen Tablet', hash: '#touchscreen' },
      ],
    },
    {
      label: 'Resume',
      path: '/resume',
      subMenu: [
        { label: 'Page 1', hash: '#page1' },
        { label: 'Page 2', hash: '#page2' },
        { label: 'Page 3', hash: '#page3' },
        { label: 'Page 4', hash: '#page4' },
      ],
    },
    {
      label: 'Contact',
      path: '/contact',
    },
  ]
  return (
    <div className={styles.haasSideNavContainer}>
      <h1 className={styles.menuHeading}>
        Anthony
        <wbr />
        Cregan
        <wbr />
        <sup>.co.uk</sup>
      </h1>
      <h6 className={styles.menuSubHeading}>Front-End & Web Application Development Portfolio</h6>
      <CommonNavMenu menuConfig={menuConfig} />
    </div>
  )
}

export default HaasSideNav
