import React from 'react'
import propTypes from 'prop-types'
import classNames from 'classnames/bind'
import PortfolioItem from '../../common/portfolioItem/portfolioItem.'
import ScrollIntoView from '../../common/scrollIntoView/scrollIntoView'
import LazyLightbox from '../../common/lazyLightbox/lazyLightbox'
import { THEME } from '../../../constants/theme'
// Images
// CatalystElements
import CatElements1 from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-1.jpg'
import CatElements1Thumb from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-Thumb-1.jpg'
import CatElements2 from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-2.jpg'
import CatElements2Thumb from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-Thumb-2.jpg'
import CatElements3 from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-3.jpg'
import CatElements3Thumb from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-Thumb-3.jpg'
import CatElements4 from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-4.jpg'
import CatElements4Thumb from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-Thumb-4.jpg'
import CatElements5 from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-5.jpg'
import CatElements5Thumb from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-Thumb-5.jpg'
import CatElements6 from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-6.jpg'
import CatElements6Thumb from '../../../public/images/screenshots/CatalystElements/Catalyst-Elements-Thumb-6.jpg'
// Cryptotron
import Cryptotron1 from '../../../public/images/screenshots/Cryptotron/Cryptotron1.jpg'
import Cryptotron1Thumb from '../../../public/images/screenshots/Cryptotron/Thumb-Cryptotron1.jpg'
import Cryptotron2 from '../../../public/images/screenshots/Cryptotron/Cryptotron2.jpg'
import Cryptotron2Thumb from '../../../public/images/screenshots/Cryptotron/Thumb-Cryptotron2.jpg'
import Cryptotron3 from '../../../public/images/screenshots/Cryptotron/Cryptotron3.jpg'
import Cryptotron3Thumb from '../../../public/images/screenshots/Cryptotron/Thumb-Cryptotron3.jpg'
import Cryptotron4 from '../../../public/images/screenshots/Cryptotron/Cryptotron4.jpg'
import Cryptotron4Thumb from '../../../public/images/screenshots/Cryptotron/Thumb-Cryptotron4.jpg'
// ReactCatalyst
import ReactCatalyst1 from '../../../public/images/screenshots/ReactCatalyst/React-Catalyst-1.jpg'
import ReactCatalyst1Thumb from '../../../public/images/screenshots/ReactCatalyst/Thumb-React-Catalyst-1.jpg'
import ReactCatalyst2 from '../../../public/images/screenshots/ReactCatalyst/React-Catalyst-2.jpg'
import ReactCatalyst2Thumb from '../../../public/images/screenshots/ReactCatalyst/Thumb-React-Catalyst-2.jpg'
import ReactCatalyst3 from '../../../public/images/screenshots/ReactCatalyst/React-Catalyst-3.jpg'
import ReactCatalyst3Thumb from '../../../public/images/screenshots/ReactCatalyst/Thumb-React-Catalyst-3.jpg'
import ReactCatalyst4 from '../../../public/images/screenshots/ReactCatalyst/React-Catalyst-4.jpg'
import ReactCatalyst4Thumb from '../../../public/images/screenshots/ReactCatalyst/Thumb-React-Catalyst-4.jpg'
// Touchscreen
import Touchscreen from '../../../public/images/screenshots/others/RaspiTouch1-image.jpg'
// ScribeCMS v2 - CMS
import Scribe2_1 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-1.jpg'
import Scribe2_1_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-1-thumb.jpg'
import Scribe2_3 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-3.jpg'
import Scribe2_3_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-3-thumb.jpg'
import Scribe2_4 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-4.jpg'
import Scribe2_4_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-4-thumb.jpg'
import Scribe2_5 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-5.jpg'
import Scribe2_5_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-5-thumb.jpg'
import Scribe2_8 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-8.jpg'
import Scribe2_8_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-8-thumb.jpg'
import Scribe2_9 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-9.jpg'
import Scribe2_9_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-9-thumb.jpg'
import Scribe2_10 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-10.jpg'
import Scribe2_10_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-10-thumb.jpg'
import Scribe2_11 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-11.jpg'
import Scribe2_11_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-11-thumb.jpg'
import Scribe2_12 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-12.jpg'
import Scribe2_12_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-12-thumb.jpg'
import Scribe2_14 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-14.jpg'
import Scribe2_14_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-14-thumb.jpg'
import Scribe2_15 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-15.jpg'
import Scribe2_15_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-15-thumb.jpg'
import Scribe2_17 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-17.jpg'
import Scribe2_17_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-17-thumb.jpg'
import Scribe2_18 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-18.jpg'
import Scribe2_18_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-18-thumb.jpg'
import Scribe2_19 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-19.jpg'
import Scribe2_19_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-19-thumb.jpg'
import Scribe2_20 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-20.jpg'
import Scribe2_20_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-20-thumb.jpg'
import Scribe2_21 from '../../../public/images/screenshots/scribe2CMS/scribeCMS-21.jpg'
import Scribe2_21_thumb from '../../../public/images/screenshots/scribe2CMS/scribeCMS-21-thumb.jpg'

// Perpetual Summer Ltd - ScribeCMS
import PerpetualSummer_1 from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-1.jpg'
import PerpetualSummer_1_thumb from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-1-thumb.jpg'
import PerpetualSummer_2 from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-2.jpg'
import PerpetualSummer_2_thumb from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-2-thumb.jpg'
import PerpetualSummer_3 from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-3.jpg'
import PerpetualSummer_3_thumb from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-3-thumb.jpg'
import PerpetualSummer_4 from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-4.jpg'
import PerpetualSummer_4_thumb from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-4-thumb.jpg'
import PerpetualSummer_5 from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-5.jpg'
import PerpetualSummer_5_thumb from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-5-thumb.jpg'
import PerpetualSummer_6 from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-6.jpg'
import PerpetualSummer_6_thumb from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-6-thumb.jpg'
import PerpetualSummer_7 from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-7.jpg'
import PerpetualSummer_7_thumb from '../../../public/images/screenshots/scribe2SITE/perpetual-summer-7-thumb.jpg'

import { ThemeNameType } from '../../../Types'

import styles from './projects.module'
let cx = classNames.bind(styles)

const Projects = ({ themeName }: ThemeNameType) : JSX.Element => {
  const currentYear = JSON.stringify(new Date().getFullYear())

  let containerClasses = cx({
    projectsContainer: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_sprawl: themeName === THEME.SPRAWL,
    theme_haas: themeName === THEME.HAAS,
  })

  return (
    <div className={containerClasses}>
      <div>
        <h1 className={styles.heading}>Projects & Open Source</h1>

        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Scribe CMS v4"
            icon={[]}
            fromDate={{ month: 'NOV', year: '2021' }}
            toDate={{ month: 'PRESENT', year: currentYear }}
            hashLink="scribe-cms"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>
                  NODE.JS Full-Stack Study Project: A Content Management System for websites using
                  Prisma, Apollo and GraphQL
                </h5>
                <p>
                  As part of the process of updating my knowledge of back-end software I decided the
                  best way to learn the latest stack would be to build a content managemnt system as
                  it uses all of the CRUD operations that would typically be required.{' '}
                </p>
                <p>
                  Previously I have used PHP and MySQL for back-end concerns however these
                  technologies have somewhat fallen out of fashion and been replaced by Node.js
                  based solutions. With that in mind I decided to build my database with SQL lite
                  using Prisma for ORM, GraphQL to query the database and Apollo on the front end to
                  access and update the data as well as keeping the state of the application in
                  sync.
                </p>
                <p>
                  As is often the case with these side-projects. What started out as a learning
                  exercise quickly turned into a fully specced out project with many months of work
                  involved. I included an image library service as part of the CMS that enables me
                  to upload and manage a library of images for use within the content of the site.
                  The WYSIWYG editor I utilised in order to edit and create articles is ReactQuill
                  which uses a custom image loader module I designed for integration with my library
                  files and database.
                </p>
                <p>
                  {
                    "For information on the first site I've developed that has content driven by the ScribeCMS, Perpetual Summer Ltd, please click "
                  }
                  <a href="projects#perpetual-summer" rel="noreferrer">
                    here
                  </a>
                  .
                </p>
              </div>
              <div className={styles.col3_imagesContainer}>
                <LazyLightbox src={Scribe2_1} thumbSrc={Scribe2_1_thumb} />
                <LazyLightbox src={Scribe2_3} thumbSrc={Scribe2_3_thumb} />
                <LazyLightbox src={Scribe2_4} thumbSrc={Scribe2_4_thumb} />
                <LazyLightbox src={Scribe2_5} thumbSrc={Scribe2_5_thumb} />
                <LazyLightbox src={Scribe2_8} thumbSrc={Scribe2_8_thumb} />
                <LazyLightbox src={Scribe2_9} thumbSrc={Scribe2_9_thumb} />
                <LazyLightbox src={Scribe2_10} thumbSrc={Scribe2_10_thumb} />
                <LazyLightbox src={Scribe2_11} thumbSrc={Scribe2_11_thumb} />
                <LazyLightbox src={Scribe2_12} thumbSrc={Scribe2_12_thumb} />
                <LazyLightbox src={Scribe2_14} thumbSrc={Scribe2_14_thumb} />
                <LazyLightbox src={Scribe2_15} thumbSrc={Scribe2_15_thumb} />
                <LazyLightbox src={Scribe2_17} thumbSrc={Scribe2_17_thumb} />
                <LazyLightbox src={Scribe2_18} thumbSrc={Scribe2_18_thumb} />
                <LazyLightbox src={Scribe2_19} thumbSrc={Scribe2_19_thumb} />
                <LazyLightbox src={Scribe2_20} thumbSrc={Scribe2_20_thumb} />
                <LazyLightbox src={Scribe2_21} thumbSrc={Scribe2_21_thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>

        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Perpetual Summer Ltd Website"
            icon={['PerpetualSummer']}
            fromDate={{ month: 'NOV', year: '2021' }}
            toDate={{ month: 'PRESENT', year: currentYear }}
            hashLink="perpetual-summer"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>
                  My contract development services company website, driven by the ScribeCMS content
                  management system.
                </h5>
                <p>
                  {'This blog-style website is driven by my custom content management system '}
                  <a href="projects#scribe-cms" rel="noreferrer">
                    ScribeCMS
                  </a>
                </p>
                <p>
                  Originally designed as a simple means of displaying the content of the ScribeCMS,
                  once the scope of that project evolved from a simple study project to a fully open
                  source CMS project I rolled it out to be the main website of my software
                  development services company, Perpetual Summer Ltd.
                </p>
                <p>
                  {
                    'As you can see from the attached screenshots, as of the time of writing it contains a series of articles on a range of disparate subjects, these are just filler content however, as the CMS gets developed further I will focus the content posted to include updates from clients and progress on my open source projects such as the aforementioned ScribeCMS as well as '
                  }
                  <a href="projects#react-catalyst" rel="noreferrer">
                    React-Catalyst
                  </a>
                  {' and the '}
                  <a href="projects#catalyst-elements" rel="noreferrer">
                    Catalyst Elements
                  </a>
                  {' UI component library.'}
                </p>
              </div>
              <div className={styles.col3_imagesContainer}>
                <LazyLightbox src={PerpetualSummer_1} thumbSrc={PerpetualSummer_1_thumb} />
                <LazyLightbox src={PerpetualSummer_2} thumbSrc={PerpetualSummer_2_thumb} />
                <LazyLightbox src={PerpetualSummer_3} thumbSrc={PerpetualSummer_3_thumb} />
                <LazyLightbox src={PerpetualSummer_4} thumbSrc={PerpetualSummer_4_thumb} />
                <LazyLightbox src={PerpetualSummer_5} thumbSrc={PerpetualSummer_5_thumb} />
                <LazyLightbox src={PerpetualSummer_6} thumbSrc={PerpetualSummer_6_thumb} />
                <LazyLightbox src={PerpetualSummer_7} thumbSrc={PerpetualSummer_7_thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>

        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Catalyst Elements"
            icon={['CatalystElements']}
            fromDate={{ month: 'MAR', year: '2021' }}
            toDate={{ month: 'PRESENT', year: currentYear }}
            hashLink="catalyst-elements"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>React Layout, Utility and UI Component Library</h5>
                <p>
                  Catalyst Elements is a collection of composable Open-source layout elements, user
                  interface components and utilities. Built with a simple consistent and common API
                  it is designed to allow the developer to create highly customisable inputs and
                  components.
                </p>
                <p>
                  This library of components was originally part of the React Catalyst development
                  environment, I split it off into its own project so that I could publish it to NPM
                  and install it as a dependency as and when required.
                </p>
                <p>
                  For more information, interactive documentation and tutorials on usage, visit the
                  Catalyst Elements website{' '}
                  <a
                    href="https://catalyst-elements.perpetualsummer.ltd/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    here
                  </a>
                  .
                </p>
              </div>
              <div className={styles.imagesContainer}>
                <LazyLightbox src={CatElements1} thumbSrc={CatElements1Thumb} />
                <LazyLightbox src={CatElements2} thumbSrc={CatElements2Thumb} />
                <LazyLightbox src={CatElements3} thumbSrc={CatElements3Thumb} />
                <LazyLightbox src={CatElements4} thumbSrc={CatElements4Thumb} />
                <LazyLightbox src={CatElements5} thumbSrc={CatElements5Thumb} />
                <LazyLightbox src={CatElements6} thumbSrc={CatElements6Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="React Catalyst"
            icon={['ReactCatalyst']}
            fromDate={{ month: 'OCT', year: '2017' }}
            toDate={{ month: 'PRESENT', year: currentYear }}
            hashLink="react-catalyst"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>Webpack based React Development Environment</h5>
                <p>
                  As part of a migration from Gulp to Webpack during my time at SIS, I learned how
                  to build a development environment with modern tooling to enable a better
                  developer experience. I developed React Catalyst as a continuation of that work to
                  enable me to quickly start developing web applications using my prefered toolset.
                  It has hot-reloading during development and features a highly optimised production
                  build system.
                </p>

                <p>
                  It is opinionated about the toolset employed as I built it for my own purposes, as
                  such it has out of the box support for a huge range of development tools and
                  libraries including React, Redux, ReactRouter, CSS Modules, Autoprefix, SCSS,
                  Layout, Animation, Typography, Linting, Testing and more. It can also be
                  configured to deploy the optimised production build via FTP for easy itterative
                  design with rapid deployment.
                </p>

                <p>
                  Over time, as I have used this on various projects its has expanded to include a
                  broad range of commonly used reusable components. It features a range of
                  controlled components for forms, buttons, links, liteboxes and thumbnails. Each of
                  these optional reusable components has been built with simple configuration and
                  optimisation in mind and are only bundled as part of the final build if utilised
                  in the project. This greatly decreases development time and provides a wealth of
                  helper components off-the-shelf without additional redundant code making it into
                  the final production build - as is often the case when using external libraries.
                </p>

                <p>
                  This website was also built using the React Catalyst Development Environment. This
                  project is open-source and the codeis publicly available on{' '}
                  <a
                    href="https://bitbucket.org/c0un7z3r0/react-catalyst-v2/src/master/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    bitbucket
                  </a>{' '}
                  . The documentation site, complete with tutorials can be found{' '}
                  <a
                    href="https://react-catalyst.perpetualsummer.ltd/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    here
                  </a>
                </p>
              </div>
              <div className={styles.twoColImagesContainer}>
                <LazyLightbox src={ReactCatalyst1} thumbSrc={ReactCatalyst1Thumb} />
                <LazyLightbox src={ReactCatalyst2} thumbSrc={ReactCatalyst2Thumb} />
                <LazyLightbox src={ReactCatalyst3} thumbSrc={ReactCatalyst3Thumb} />
                <LazyLightbox src={ReactCatalyst4} thumbSrc={ReactCatalyst4Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Cryptotron"
            icon={['Bitcoin']}
            fromDate={{ month: 'MAR', year: '2020' }}
            toDate={{ month: 'JUN', year: '2020' }}
            hashLink="cryptotron"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>Cryptocurrencies Market Data App</h5>
                <p>
                  A Cryptocurrencies data visualisation app that started as a side-project as a
                  means to learn d3, the very popular data visualisation library.
                </p>
                <p>
                  It consumes public Cryptocurrencies API data and converts it into line graphs,
                  candle charts and pie charts to display price data, trading data and market
                  capacity distribution information.
                </p>
              </div>
              <div className={styles.twoColImagesContainer}>
                <LazyLightbox src={Cryptotron1} thumbSrc={Cryptotron1Thumb} />
                <LazyLightbox src={Cryptotron2} thumbSrc={Cryptotron2Thumb} />
                <LazyLightbox src={Cryptotron3} thumbSrc={Cryptotron3Thumb} />
                <LazyLightbox src={Cryptotron4} thumbSrc={Cryptotron4Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Touchscreen Tablet"
            icon={['RaspberryPi']}
            fromDate={{ month: 'JAN', year: '2018' }}
            toDate={{ month: 'MAR', year: '2018' }}
            hashLink="touchscreen"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>Home Automation Project</h5>
                <p>
                  Built on NodeJS and running on a Raspberry Pi SoC board connected to a touchscreen
                  display, this project enables the user to get the weather forecast, control
                  spotify music and playlists, view and edit their calendar with reminders and also
                  control and monitor IoT lighting throughout the house.
                </p>
                <p>
                  The UI is built with react.js served with Webpack via a locally running express
                  server and headless chromium instance. I had planned on building on it with more
                  sensors, heating control and a home CCTV system. I had built the homemade cameras
                  using the raspberry pi camera module but have not integrated that feature into the
                  device.
                </p>
                <p>
                  I built this project as I enjoy working with electronics and hardware and to
                  familiarise myself more with NodeJS.
                </p>
              </div>
              <div className={styles.singleColImagesContainer}>
                <LazyLightbox src={Touchscreen} thumbSrc={Touchscreen} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
      </div>
    </div>
  )
}

Projects.propTypes = {
  themeName: propTypes.string.isRequired,
}
export default Projects
