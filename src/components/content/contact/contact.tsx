import React from 'react'
import propTypes from 'prop-types'
import classNames from 'classnames/bind'
import { THEME } from '../../../constants/theme'
import SvgIcon from '../../common/svgIcon/svgIcon'

import { ThemeNameType } from '../../../Types'

import styles from './contact.module'
let cx = classNames.bind(styles)

const Contact = ({ themeName }: ThemeNameType) : JSX.Element => {

  let containerClasses = cx({
    pageContainer: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_haas: themeName === THEME.HAAS,
  })

  return (
    <div className={containerClasses}>
      <div className={styles.contactContainer}>
        <div className={styles.tel}>
          <span className={styles.icon}>
            <SvgIcon image="Telephone" />
          </span>{' '}
          <span className={styles.text}>
            <a href="tel:6596133184">+65 96133184</a>
          </span>
        </div>
        <div className={styles.email}>
          <span className={styles.icon}>
            <SvgIcon image="Email" />
          </span>
          <span className={styles.text}>
            <a href="mailto:hello@anthonycregan.co.uk">hello@anthonycregan.co.uk</a>
          </span>
        </div>
        <div className={styles.stackOverflow}>
          <span className={styles.icon}>
            <SvgIcon image="StackOverflow" />
          </span>
          <span className={styles.text}>
            <a
              href="https://stackoverflow.com/users/3626334/anthony-cregan"
              target="_blank"
              rel="noreferrer"
            >
              Stack Overflow Profile
            </a>
          </span>
        </div>
        <div className={styles.linkedIn}>
          <span className={styles.icon}>
            <SvgIcon image="LinkedIn" />
          </span>
          <span className={styles.text}>
            <a
              href="https://www.linkedin.com/in/anthony-cregan-64965267/"
              target="_blank"
              rel="noreferrer"
            >
              LinkedIn Profile
            </a>
          </span>
        </div>
      </div>
    </div>
  )
}

Contact.propTypes = {
  themeName: propTypes.string.isRequired,
}

export default Contact
