import React from 'react'
import propTypes from 'prop-types'
import classNames from 'classnames/bind'
import { THEME } from '../../../constants/theme'

import { ThemeNameType } from '../../../Types'

import styles from './home.module'
let cx = classNames.bind(styles)

const Home = ({ themeName }: ThemeNameType) : JSX.Element => {
  let containerClasses = cx({
    theme_artDeco_homeContainer: themeName === THEME.ARTDECO,
    theme_original_homeContainer: themeName === THEME.ORIGINAL,
    theme_santaCruz_homeContainer: themeName === THEME.SANTACRUZ,
    theme_sprawl_homeContainer: themeName === THEME.SPRAWL,
    theme_haas_homeContainer: themeName === THEME.HAAS,
  })

  return (
    <div className={containerClasses}>
      {themeName === THEME.HAAS && <div className={styles.backgroundTile} />}
      <h1 className={styles.title}>
        <span>
          Anthony
          <wbr />
          Cregan<sup>.co.uk</sup>
        </span>
      </h1>
      <h3 className={styles.summary}>
        <span>Specialist Front-End & UI Software Developer</span>
      </h3>
    </div>
  )
}

Home.propTypes = {
  themeName: propTypes.string.isRequired,
}

export default Home
