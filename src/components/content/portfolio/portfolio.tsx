import React from 'react'
import propTypes from 'prop-types'
import classNames from 'classnames/bind'
import { Link } from 'react-router-dom'
// @ts-ignore
import Video from '@perpetualsummer/catalyst-elements/Video'
import { THEME } from '../../../constants/theme'
import PortfolioItem from '../../common/portfolioItem/portfolioItem.'
import LazyLightbox from '../../common/lazyLightbox/lazyLightbox'
import ScrollIntoView from '../../common/scrollIntoView/scrollIntoView'
//images & videos
// ALEPH
import aleph1 from '../../../public/images/screenshots/aleph/Dashboard.jpg'
import aleph1Thumb from '../../../public/images/screenshots/aleph/Dashboard-thumb.jpg'
import aleph2 from '../../../public/images/screenshots/aleph/Menu.jpg'
import aleph2Thumb from '../../../public/images/screenshots/aleph/Menu-thumb.jpg'
import aleph3 from '../../../public/images/screenshots/aleph/Message-Center.jpg'
import aleph3Thumb from '../../../public/images/screenshots/aleph/Message-Center-thumb.jpg'
import aleph4 from '../../../public/images/screenshots/aleph/Payment.jpg'
import aleph4Thumb from '../../../public/images/screenshots/aleph/Payment-thumb.jpg'
import aleph5 from '../../../public/images/screenshots/aleph/Summary.jpg'
import aleph5Thumb from '../../../public/images/screenshots/aleph/Summary-thumb.jpg'

// UOB SUSTAINABILITY
import uobSus0 from '../../../public/images/screenshots/uob_sus/UOB_SUS_0.png'
import uobSus0Thumb from '../../../public/images/screenshots/uob_sus/UOB_SUS_0_THUMB.png'
import uobSus1 from '../../../public/images/screenshots/uob_sus/UOB_SUS_1.png'
import uobSus1Thumb from '../../../public/images/screenshots/uob_sus/UOB_SUS_1_THUMB.png'
import uobSus2 from '../../../public/images/screenshots/uob_sus/UOB_SUS_2.png'
import uobSus2Thumb from '../../../public/images/screenshots/uob_sus/UOB_SUS_2_THUMB.png'
import uobSus3 from '../../../public/images/screenshots/uob_sus/UOB_SUS_3.png'
import uobSus3Thumb from '../../../public/images/screenshots/uob_sus/UOB_SUS_3_THUMB.png'
import uobSus4 from '../../../public/images/screenshots/uob_sus/UOB_SUS_4.png'
import uobSus4Thumb from '../../../public/images/screenshots/uob_sus/UOB_SUS_4_THUMB.png'
import uobSus5 from '../../../public/images/screenshots/uob_sus/UOB_SUS_5.png'
import uobSus5Thumb from '../../../public/images/screenshots/uob_sus/UOB_SUS_5_THUMB.png'

import uobSus0Mobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_0.png'
import uobSus0ThumbMobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_0_THUMB.png'
import uobSus1Mobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_1.png'
import uobSus1ThumbMobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_1_THUMB.png'
import uobSus2Mobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_2.png'
import uobSus2ThumbMobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_2_THUMB.png'
import uobSus3Mobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_3.png'
import uobSus3ThumbMobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_3_THUMB.png'
import uobSus4Mobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_4.png'
import uobSus4ThumbMobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_4_THUMB.png'
import uobSus5Mobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_5.png'
import uobSus5ThumbMobile from '../../../public/images/screenshots/uob_sus/UOB_SUS_MOBILE_5_THUMB.png'

// UOB
import uobHero from '../../../public/images/screenshots/uob/United-Overseas-Bank-Infinity-Hero.jpg'
import uobHeroThumb from '../../../public/images/screenshots/uob/United-Overseas-Bank-Infinity-Hero-thumb.jpg'
import uobInfinityDashboard from '../../../public/images/screenshots/uob/UOB-Infinity-Dashboard.jpg'
import uobInfinityDashboardThumb from '../../../public/images/screenshots/uob/UOB-Infinity-Dashboard-thumb.jpg'
import uob1 from '../../../public/images/screenshots/uob/UOB-1.jpg'
import uob1Thumb from '../../../public/images/screenshots/uob/UOB-1-thumb.jpg'
import uob2 from '../../../public/images/screenshots/uob/UOB-2.jpg'
import uob2Thumb from '../../../public/images/screenshots/uob/UOB-2-thumb.jpg'
import uob3 from '../../../public/images/screenshots/uob/UOB-3.jpg'
import uob3Thumb from '../../../public/images/screenshots/uob/UOB-3-thumb.jpg'
import uob4 from '../../../public/images/screenshots/uob/UOB-4.jpg'
import uob4Thumb from '../../../public/images/screenshots/uob/UOB-4-thumb.jpg'
import uob5 from '../../../public/images/screenshots/uob/UOB-5.jpg'
import uob5Thumb from '../../../public/images/screenshots/uob/UOB-5-thumb.jpg'
import uob6 from '../../../public/images/screenshots/uob/UOB-6.jpg'
import uob6Thumb from '../../../public/images/screenshots/uob/UOB-6-thumb.jpg'
import uob7 from '../../../public/images/screenshots/uob/UOB-7.jpg'
import uob7Thumb from '../../../public/images/screenshots/uob/UOB-7-thumb.jpg'
import uob8 from '../../../public/images/screenshots/uob/UOB-8.jpg'
import uob8Thumb from '../../../public/images/screenshots/uob/UOB-8-thumb.jpg'
import uob9 from '../../../public/images/screenshots/uob/UOB-9.jpg'
import uob9Thumb from '../../../public/images/screenshots/uob/UOB-9-thumb.jpg'
// BOOKING
import booking0 from '../../../public/images/screenshots/booking.com/booking-0.jpg'
import booking0Thumb from '../../../public/images/screenshots/booking.com/booking-0-thumb.jpg'
import booking1 from '../../../public/images/screenshots/booking.com/booking-1.jpg'
import booking1Thumb from '../../../public/images/screenshots/booking.com/booking-1-thumb.jpg'
import booking2 from '../../../public/images/screenshots/booking.com/booking-2.jpg'
import booking2Thumb from '../../../public/images/screenshots/booking.com/booking-2-thumb.jpg'
import booking3 from '../../../public/images/screenshots/booking.com/booking-3.jpg'
import booking3Thumb from '../../../public/images/screenshots/booking.com/booking-3-thumb.jpg'
import booking4 from '../../../public/images/screenshots/booking.com/booking-4.jpg'
import booking4Thumb from '../../../public/images/screenshots/booking.com/booking-4-thumb.jpg'
import booking6 from '../../../public/images/screenshots/booking.com/booking-6.jpg'
import booking6Thumb from '../../../public/images/screenshots/booking.com/booking-6-thumb.jpg'
import booking7 from '../../../public/images/screenshots/booking.com/booking-7.jpg'
import booking7Thumb from '../../../public/images/screenshots/booking.com/booking-7-thumb.jpg'
import booking8 from '../../../public/images/screenshots/booking.com/booking-8.jpg'
import booking8Thumb from '../../../public/images/screenshots/booking.com/booking-8-thumb.jpg'
import booking9 from '../../../public/images/screenshots/booking.com/booking-9.jpg'
import booking9Thumb from '../../../public/images/screenshots/booking.com/booking-9-thumb.jpg'
import booking10 from '../../../public/images/screenshots/booking.com/booking-10.jpg'
import booking10Thumb from '../../../public/images/screenshots/booking.com/booking-10-thumb.jpg'
import booking11 from '../../../public/images/screenshots/booking.com/booking-11.jpg'
import booking11Thumb from '../../../public/images/screenshots/booking.com/booking-11-thumb.jpg'
// AE LIVE
import ae1 from '../../../public/images/screenshots/ae/AEG-1.jpg'
import ae1Thumb from '../../../public/images/screenshots/ae/AEG-1-thumb.jpg'
import ae2 from '../../../public/images/screenshots/ae/AEG-2.jpg'
import ae2Thumb from '../../../public/images/screenshots/ae/AEG-2-thumb.jpg'
import ae3 from '../../../public/images/screenshots/ae/AEG-3.jpg'
import ae3Thumb from '../../../public/images/screenshots/ae/AEG-3-thumb.jpg'
import aeVideoAvi from '../../../public/videos/ae/overlay1080.avi'
import aeVideoMov from '../../../public/videos/ae/overlay1080.mov'
import aeVideoMp4 from '../../../public/videos/ae/overlay1080.mp4'
import aeVideoOgg from '../../../public/videos/ae/overlay1080.ogg'
import aeVideoWebm from '../../../public/videos/ae/overlay1080.webm'
// BETFRED
import betfredImage from '../../../public/images/screenshots/betfred/betfred-iphone8-image.gif'
// SIS
// 24hr Greyhounds
import greyhounds1 from '../../../public/images/screenshots/sis/VideoOverlays/FORM.jpg'
import greyhounds1Thumb from '../../../public/images/screenshots/sis/VideoOverlays/FORM-thumb.jpg'
import greyhounds2 from '../../../public/images/screenshots/sis/VideoOverlays/PRERACE.jpg'
import greyhounds2Thumb from '../../../public/images/screenshots/sis/VideoOverlays/PRERACE-thumb.jpg'
import greyhounds3 from '../../../public/images/screenshots/sis/VideoOverlays/PRICE.jpg'
import greyhounds3Thumb from '../../../public/images/screenshots/sis/VideoOverlays/PRICE-thumb.jpg'
import greyhounds4 from '../../../public/images/screenshots/sis/VideoOverlays/INRACE.jpg'
import greyhounds4Thumb from '../../../public/images/screenshots/sis/VideoOverlays/INRACE-thumb.jpg'
import greyhounds5 from '../../../public/images/screenshots/sis/VideoOverlays/INRACE2.jpg'
import greyhounds5Thumb from '../../../public/images/screenshots/sis/VideoOverlays/INRACE2-thumb.jpg'
import greyhounds6 from '../../../public/images/screenshots/sis/VideoOverlays/RESULT.jpg'
import greyhounds6Thumb from '../../../public/images/screenshots/sis/VideoOverlays/RESULT-thumb.jpg'
import greyhoundsVideo from '../../../public/videos/sis/SISGreyhoundChannelDemoVideo.mp4'
// RunnerDB
import runnerDb1 from '../../../public/images/screenshots/sis/RunnerDB/Homepage.jpg'
import runnerDb1Thumb from '../../../public/images/screenshots/sis/RunnerDB/Homepage-thumb.jpg'
import runnerDb2 from '../../../public/images/screenshots/sis/RunnerDB/Welcome.jpg'
import runnerDb2Thumb from '../../../public/images/screenshots/sis/RunnerDB/Welcome-thumb.jpg'
import runnerDb3 from '../../../public/images/screenshots/sis/RunnerDB/LiveRaces.jpg'
import runnerDb3Thumb from '../../../public/images/screenshots/sis/RunnerDB/LiveRaces-thumb.jpg'
import runnerDb4 from '../../../public/images/screenshots/sis/RunnerDB/Runners.jpg'
import runnerDb4Thumb from '../../../public/images/screenshots/sis/RunnerDB/Runners-thumb.jpg'
import runnerDb5 from '../../../public/images/screenshots/sis/RunnerDB/TrackGeo1.jpg'
import runnerDb5Thumb from '../../../public/images/screenshots/sis/RunnerDB/TrackGeo1-thumb.jpg'
import runnerDb6 from '../../../public/images/screenshots/sis/RunnerDB/TrackGeo2.jpg'
import runnerDb6Thumb from '../../../public/images/screenshots/sis/RunnerDB/TrackGeo2-thumb.jpg'
// Streaming Access Control
import sac1 from '../../../public/images/screenshots/sis/StreamingAccessControl/channel-image.jpg'
import sac1thumb from '../../../public/images/screenshots/sis/StreamingAccessControl/channel-thumb.jpg'
import sac2 from '../../../public/images/screenshots/sis/StreamingAccessControl/customers-image.jpg'
import sac2thumb from '../../../public/images/screenshots/sis/StreamingAccessControl/customers-thumb.jpg'
import sac3 from '../../../public/images/screenshots/sis/StreamingAccessControl/filter-image.jpg'
import sac3thumb from '../../../public/images/screenshots/sis/StreamingAccessControl/filter-thumb.jpg'
import sac4 from '../../../public/images/screenshots/sis/StreamingAccessControl/schedule-image.jpg'
import sac4thumb from '../../../public/images/screenshots/sis/StreamingAccessControl/schedule-thumb.jpg'
import sac5 from '../../../public/images/screenshots/sis/StreamingAccessControl/service-image.jpg'
import sac5thumb from '../../../public/images/screenshots/sis/StreamingAccessControl/service-thumb.jpg'
import sac6 from '../../../public/images/screenshots/sis/StreamingAccessControl/widget-image.jpg'
import sac6thumb from '../../../public/images/screenshots/sis/StreamingAccessControl/widget-thumb.jpg'
// Dynamic Racecards
import racecards1 from '../../../public/images/screenshots/sis/DynamicRacecards/InRunning-Image.jpg'
import racecards1Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/InRunning-Thumb.jpg'
import racecards2 from '../../../public/images/screenshots/sis/DynamicRacecards/InRunning2-Image.jpg'
import racecards2Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/InRunning2-Thumb.jpg'
import racecards3 from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing1-Image.jpg'
import racecards3Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing1-Thumb.jpg'
import racecards4 from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing2-Image.jpg'
import racecards4Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing2-Thumb.jpg'
import racecards5 from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing3-Image.jpg'
import racecards5Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing3-Thumb.jpg'
import racecards6 from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing4-Image.jpg'
import racecards6Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/IntRacing4-Thumb.jpg'
import racecards7 from '../../../public/images/screenshots/sis/DynamicRacecards/MDRC1-Image.jpg'
import racecards7Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/MDRC1-Thumb.jpg'
import racecards8 from '../../../public/images/screenshots/sis/DynamicRacecards/Multiview-1.jpg'
import racecards8Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/Multiview-1-thumb.jpg'
import racecards9 from '../../../public/images/screenshots/sis/DynamicRacecards/Multiview-2.jpg'
import racecards9Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/Multiview-2-thumb.jpg'
import racecards10 from '../../../public/images/screenshots/sis/DynamicRacecards/RevisionPrototype-Image.jpg'
import racecards10Thumb from '../../../public/images/screenshots/sis/DynamicRacecards/RevisionPrototype-Thumb.jpg'
// Football Services
import football1 from '../../../public/images/screenshots/sis/FootballServices/Home.jpg'
import football1Thumb from '../../../public/images/screenshots/sis/FootballServices/Home-thumb.jpg'
import football2 from '../../../public/images/screenshots/sis/FootballServices/Matches.jpg'
import football2Thumb from '../../../public/images/screenshots/sis/FootballServices/Matches-thumb.jpg'
import football3 from '../../../public/images/screenshots/sis/FootballServices/unnamed1.jpg'
import football3Thumb from '../../../public/images/screenshots/sis/FootballServices/unnamed1-Thumb.jpg'
import football4 from '../../../public/images/screenshots/sis/FootballServices/unnamed2.jpg'
import football4Thumb from '../../../public/images/screenshots/sis/FootballServices/unnamed2-Thumb.jpg'
import football5 from '../../../public/images/screenshots/sis/FootballServices/unnamed3.jpg'
import football5Thumb from '../../../public/images/screenshots/sis/FootballServices/unnamed3-Thumb.jpg'
//OTHER PROJECTS
// TDCbitworks
import tdc1 from '../../../public/images/screenshots/tdcbitworks/tdc-slide-1.jpg'
import tdc1thumb from '../../../public/images/screenshots/tdcbitworks/tdc-slide-1-thumb.jpg'
import tdc2 from '../../../public/images/screenshots/tdcbitworks/tdc-slide-2.jpg'
import tdc2thumb from '../../../public/images/screenshots/tdcbitworks/tdc-slide-2-thumb.jpg'
import tdc3 from '../../../public/images/screenshots/tdcbitworks/tdc-slide-3.jpg'
import tdc3thumb from '../../../public/images/screenshots/tdcbitworks/tdc-slide-3-thumb.jpg'
import tdc4 from '../../../public/images/screenshots/tdcbitworks/tdc-slide-4.jpg'
import tdc4thumb from '../../../public/images/screenshots/tdcbitworks/tdc-slide-4-thumb.jpg'
import tdc5 from '../../../public/images/screenshots/tdcbitworks/tdc-slide-5.jpg'
import tdc5thumb from '../../../public/images/screenshots/tdcbitworks/tdc-slide-5-thumb.jpg'
import tdc6 from '../../../public/images/screenshots/tdcbitworks/tdc-slide-6.jpg'
import tdc6thumb from '../../../public/images/screenshots/tdcbitworks/tdc-slide-6-thumb.jpg'
// ScribeCMS
import scribe1 from '../../../public/images/screenshots/scribe/scribe-slide-1.jpg'
import scribe1thumb from '../../../public/images/screenshots/scribe/scribe-slide-1-THUMB.jpg'
import scribe2 from '../../../public/images/screenshots/scribe/scribe-slide-2.jpg'
import scribe2thumb from '../../../public/images/screenshots/scribe/scribe-slide-2-THUMB.jpg'
import scribe3 from '../../../public/images/screenshots/scribe/scribe-slide-3.jpg'
import scribe3thumb from '../../../public/images/screenshots/scribe/scribe-slide-3-THUMB.jpg'
import scribe4 from '../../../public/images/screenshots/scribe/scribe-slide-4.jpg'
import scribe4thumb from '../../../public/images/screenshots/scribe/scribe-slide-4-THUMB.jpg'
import scribe5 from '../../../public/images/screenshots/scribe/scribe-slide-5.jpg'
import scribe5thumb from '../../../public/images/screenshots/scribe/scribe-slide-5-THUMB.jpg'
import scribe6 from '../../../public/images/screenshots/scribe/scribe-slide-6.jpg'
import scribe6thumb from '../../../public/images/screenshots/scribe/scribe-slide-6-THUMB.jpg'
import scribe7 from '../../../public/images/screenshots/scribe/scribe-slide-7.jpg'
import scribe7thumb from '../../../public/images/screenshots/scribe/scribe-slide-7-THUMB.jpg'
import scribe8 from '../../../public/images/screenshots/scribe/scribe-slide-8.jpg'
import scribe8thumb from '../../../public/images/screenshots/scribe/scribe-slide-8-THUMB.jpg'
import scribe9 from '../../../public/images/screenshots/scribe/scribe-slide-9.jpg'
import scribe9thumb from '../../../public/images/screenshots/scribe/scribe-slide-9-THUMB.jpg'
import scribe10 from '../../../public/images/screenshots/scribe/scribe-slide-10.jpg'
import scribe10thumb from '../../../public/images/screenshots/scribe/scribe-slide-10-THUMB.jpg'
import scribe11 from '../../../public/images/screenshots/scribe/scribe-slide-11.jpg'
import scribe11thumb from '../../../public/images/screenshots/scribe/scribe-slide-11-THUMB.jpg'
import scribe12 from '../../../public/images/screenshots/scribe/scribe-slide-12.jpg'
import scribe12thumb from '../../../public/images/screenshots/scribe/scribe-slide-12-THUMB.jpg'
// Microcosmic
import microcosmic1 from '../../../public/images/screenshots/microcosmic/mc-slide-1.jpg'
import microcosmic1Thumb from '../../../public/images/screenshots/microcosmic/mc-slide-1-thumb.jpg'
import microcosmic2 from '../../../public/images/screenshots/microcosmic/mc-slide-2.jpg'
import microcosmic2Thumb from '../../../public/images/screenshots/microcosmic/mc-slide-2-thumb.jpg'
import microcosmic3 from '../../../public/images/screenshots/microcosmic/mc-slide-3.jpg'
import microcosmic3Thumb from '../../../public/images/screenshots/microcosmic/mc-slide-3-thumb.jpg'
import microcosmic4 from '../../../public/images/screenshots/microcosmic/mc-slide-4.jpg'
import microcosmic4Thumb from '../../../public/images/screenshots/microcosmic/mc-slide-4-thumb.jpg'
import microcosmic5 from '../../../public/images/screenshots/microcosmic/mc-slide-5.jpg'
import microcosmic5Thumb from '../../../public/images/screenshots/microcosmic/mc-slide-5-thumb.jpg'
import microcosmic6 from '../../../public/images/screenshots/microcosmic/mc-slide-6.jpg'
import microcosmic6Thumb from '../../../public/images/screenshots/microcosmic/mc-slide-6-thumb.jpg'
// Needles Park
import needles1 from '../../../public/images/screenshots/needles/np-slide-1.jpg'
import needles1Thumb from '../../../public/images/screenshots/needles/np-slide-1-thumb.jpg'
import needles2 from '../../../public/images/screenshots/needles/np-slide-2.jpg'
import needles2Thumb from '../../../public/images/screenshots/needles/np-slide-2-thumb.jpg'
import needles3 from '../../../public/images/screenshots/needles/np-slide-3.jpg'
import needles3Thumb from '../../../public/images/screenshots/needles/np-slide-3-thumb.jpg'
import needles4 from '../../../public/images/screenshots/needles/np-slide-4.jpg'
import needles4Thumb from '../../../public/images/screenshots/needles/np-slide-4-thumb.jpg'
import needles5 from '../../../public/images/screenshots/needles/np-slide-5.jpg'
import needles5Thumb from '../../../public/images/screenshots/needles/np-slide-5-thumb.jpg'
import needles6 from '../../../public/images/screenshots/needles/np-slide-6.jpg'
import needles6Thumb from '../../../public/images/screenshots/needles/np-slide-6-thumb.jpg'
// Snowdon Mountain Railway
import snowdon1 from '../../../public/images/screenshots/snowdon/smr-slide-1.jpg'
import snowdon1Thumb from '../../../public/images/screenshots/snowdon/smr-slide-1-THUMB.jpg'
import snowdon2 from '../../../public/images/screenshots/snowdon/smr-slide-2.jpg'
import snowdon2Thumb from '../../../public/images/screenshots/snowdon/smr-slide-2-THUMB.jpg'
import snowdon3 from '../../../public/images/screenshots/snowdon/smr-slide-3.jpg'
import snowdon3Thumb from '../../../public/images/screenshots/snowdon/smr-slide-3-THUMB.jpg'
// Skills Solutions
import skills1 from '../../../public/images/screenshots/skills/skills-slide-1.jpg'
import skills1Thumb from '../../../public/images/screenshots/skills/skills-slide-1-THUMB.jpg'
import skills2 from '../../../public/images/screenshots/skills/skills-slide-2.jpg'
import skills2Thumb from '../../../public/images/screenshots/skills/skills-slide-2-THUMB.jpg'
import skills3 from '../../../public/images/screenshots/skills/skills-slide-3.jpg'
import skills3Thumb from '../../../public/images/screenshots/skills/skills-slide-3-THUMB.jpg'
// Pension Claims Expert
import pension1 from '../../../public/images/screenshots/pension/pce-slide-1.jpg'
import pension1Thumb from '../../../public/images/screenshots/pension/pce-slide-1-thumb.jpg'
import pension2 from '../../../public/images/screenshots/pension/pce-slide-2.jpg'
import pension2Thumb from '../../../public/images/screenshots/pension/pce-slide-2-thumb.jpg'
import pension3 from '../../../public/images/screenshots/pension/pce-slide-3.jpg'
import pension3Thumb from '../../../public/images/screenshots/pension/pce-slide-3-thumb.jpg'
// Others
import enhanced from '../../../public/images/screenshots/others/eaq-slide.jpg'
import enhancedThumb from '../../../public/images/screenshots/others/eaq-slide-thumb.jpg'
import donelan from '../../../public/images/screenshots/others/td-slide.jpg'
import donelanThumb from '../../../public/images/screenshots/others/td-slide-thumb.jpg'

import { ThemeNameType } from '../../../Types'

import styles from './portfolio.module'
let cx = classNames.bind(styles)

const Portfolio = ({ themeName }: ThemeNameType) : JSX.Element => {
  let containerClasses = cx({
    portfolioContainer: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_sprawl: themeName === THEME.SPRAWL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_haas: themeName === THEME.HAAS,
  })

  const currentYear = JSON.stringify(new Date().getFullYear())

  const aeVideoArray = [
    { size: 'xl', url: aeVideoWebm, mimeType: 'video/webm' },
    { size: 'xl', url: aeVideoMp4, mimeType: 'video/mp4' },
    { size: 'xl', url: aeVideoOgg, mimeType: 'video/ogg' },
    { size: 'xl', url: aeVideoMov, mimeType: 'video/mov' },
    { size: 'xl', url: aeVideoAvi, mimeType: 'video/avi' },
    { size: 'default', url: aeVideoWebm, mimeType: 'video/webm' },
  ]

  const sisVideoArray = [
    { size: 'xl', url: greyhoundsVideo, mimeType: 'video/mp4' },
    { size: 'default', url: greyhoundsVideo, mimeType: 'video/mp4' },
  ]

  return (
    <div className={containerClasses}>
      <div>
        <h1 className={styles.heading}>Portfolio</h1>

        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Aleph Labs"
            icon={['Aleph']}
            fromDate={{ month: 'AUG', year: '2021' }}
            toDate={{ month: 'PRESENT', year: currentYear }}
            hashLink="aleph"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <p>
                  In August 2021 I started a new role at a full-service development agency called
                  Aleph-Labs. They have a presence throughout many countries in the APAC region
                  including Thailand, Australia, Indonesia, Malaysia, Philippines and Vietnam. I am
                  based in their Singapore office in a converted period shop-house in River Valley.
                </p>
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>

        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="UOB Sustainability Project"
            icon={['UOBlogo']}
            fromDate={{ month: 'JUN', year: '2022' }}
            toDate={{ month: 'OCT', year: '2022' }}
            hashLink="uob-sustainability"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>Zero-Carbon Business Questionnaire Microsite</h5>
                <p>
                  My second project at <strong>Aleph-Labs</strong> was working with the design team
                  to create a questionnaire micro-site that enables UOB to collect information from
                  their business banking clients and make recommendations as to how they can best
                  adapt to a zero carbon future.
                </p>
                <p>
                  The user journey consists of an introduction page that gives a breif overview of
                  what they can expect from completing the 5 minute questionnaire. When the user
                  clicks the &apos;Take the Quiz&apos; button they are automatically scrolled down
                  to the first question.
                </p>
                <p>
                  After choosing which sector of business the user is in on the first question, all
                  but one of the subsequent questions are simple &apos;Yes&apos; or &apos;No&apos;
                  answers. As the user progresses each answer is used to compute a score and
                  determine which of a series of PDFs they will receive at the end of the process.
                </p>
                <p>
                  The scrolling is controlled by the application. When an answer is given the user
                  is automatically scrolled to the next question. The user can scroll back using the
                  Previous button on desktop or using a swipe gesture on mobile or tablet.
                </p>
                <p>
                  If the user gets to the end of the form without answering all questions they are
                  presented with a link that scrolls them back up to the unanswered question. If the
                  user has successfully completed all questions then they are given a link to the
                  next page which will present the user with the approriate PDF with advice and
                  guidance on how to best prepare their business to adapt and provide for a green
                  future.
                </p>
              </div>
              <div className={styles.sisImagesContainer}>
                <LazyLightbox src={uobSus0} thumbSrc={uobSus0Thumb} />
                <LazyLightbox src={uobSus1} thumbSrc={uobSus1Thumb} />
                <LazyLightbox src={uobSus2} thumbSrc={uobSus2Thumb} />
                <LazyLightbox src={uobSus3} thumbSrc={uobSus3Thumb} />
                <LazyLightbox src={uobSus4} thumbSrc={uobSus4Thumb} />
                <LazyLightbox src={uobSus5} thumbSrc={uobSus5Thumb} />
                <LazyLightbox src={uobSus0Mobile} thumbSrc={uobSus0ThumbMobile} />
                <LazyLightbox src={uobSus1Mobile} thumbSrc={uobSus1ThumbMobile} />
                <LazyLightbox src={uobSus2Mobile} thumbSrc={uobSus2ThumbMobile} />
                <LazyLightbox src={uobSus3Mobile} thumbSrc={uobSus3ThumbMobile} />
                <LazyLightbox src={uobSus4Mobile} thumbSrc={uobSus4ThumbMobile} />
                <LazyLightbox src={uobSus5Mobile} thumbSrc={uobSus5ThumbMobile} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>

        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Standard Chartered Bank"
            icon={['StandardChartered']}
            fromDate={{ month: 'AUG', year: '2021' }}
            toDate={{ month: 'SEP', year: '2022' }}
            hashLink="standard-chartered"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>Ember to React.js Migration</h5>
                <p>
                  My first project at <strong>Aleph-Labs</strong> was working with their long-time
                  client, Standard Chartered Bank. Aleph has been tasked with updating SCB&apos;s
                  online presence to move their existing online-banking app away from using Ember to
                  operating solely in React.js.
                </p>
                <p>
                  Migrating to a component driven model using Module Federation microservices we are
                  currently building the UI in React using Redux for state management. The process
                  involves reverse-engineering the Ember application and splitting the exising
                  application into logical composable modules to be developed in React.
                </p>
              </div>
              <div className={styles.sisImagesContainer}>
                <LazyLightbox src={aleph1} thumbSrc={aleph1Thumb} />
                <LazyLightbox src={aleph2} thumbSrc={aleph2Thumb} />
                <LazyLightbox src={aleph5} thumbSrc={aleph5Thumb} />
                <LazyLightbox src={aleph4} thumbSrc={aleph4Thumb} />
                <LazyLightbox src={aleph3} thumbSrc={aleph3Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="United Overseas Bank"
            icon={['UOBlogo']}
            fromDate={{ month: 'JUNE', year: '2020' }}
            toDate={{ month: 'AUG', year: '2021' }}
            hashLink="uob"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>UOB INFINITY - Online Banking Portal Application</h5>
                <p>
                  In January 2020 I moved to Singapore. I started working at UOB as a Senior
                  Developer Front End in July 2020 working on their online-banking portal, UOB
                  Infinity. The scope of this role was to develop the Hong Kong variant of their
                  core banking application.
                </p>

                <p>
                  This involved identifying, planning and implementing the changes required to
                  modify the base application, which was designed for the Singapore market and adapt
                  it to accommodate the differences in the Hong Kong market. This includes changes
                  required to currency, forex, transaction types and banking facilities that are
                  unique or specific to the Hong Kong banking sector.
                </p>

                <p>
                  Due to the breadth of changes required across all areas of the application I
                  gained a great deal of experience as we were not focused on one specific area of
                  the app. I also took responsibility for adapting and expanding their core UI
                  component set to facilitate the additional features that were required to
                  accommodate the Hong Kong variant.
                </p>

                <p>
                  Having previously had no experience in the banking sector I found the work at UOB
                  rewarding and enjoyed the challenge of having to understand the obstacles unique
                  to the finance industry and how we can develop software and user interface
                  solutions to overcome them.
                </p>
              </div>
              <div className={styles.imagesContainer}>
                <LazyLightbox src={uobHero} thumbSrc={uobHeroThumb} />
                <LazyLightbox src={uobInfinityDashboard} thumbSrc={uobInfinityDashboardThumb} />
                <LazyLightbox src={uob9} thumbSrc={uob9Thumb} />
                <LazyLightbox src={uob1} thumbSrc={uob1Thumb} />
                <LazyLightbox src={uob2} thumbSrc={uob2Thumb} />
                <LazyLightbox src={uob3} thumbSrc={uob3Thumb} />
                <LazyLightbox src={uob4} thumbSrc={uob4Thumb} />
                <LazyLightbox src={uob5} thumbSrc={uob5Thumb} />
                <LazyLightbox src={uob6} thumbSrc={uob6Thumb} />
                <LazyLightbox src={uob7} thumbSrc={uob7Thumb} />
                <LazyLightbox src={uob8} thumbSrc={uob8Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Booking.com"
            icon={['booking.com', 'ryanair']}
            fromDate={{ month: 'JULY', year: '2019' }}
            toDate={{ month: 'DEC', year: '2019' }}
            hashLink="booking"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>RyanAir Car Hire Integration Project</h5>
                <p>
                  Operating for my contracting company, Perpetual Summer Ltd, I was contracted to
                  work for Booking.com in a development consultancy capacity. I provided support and
                  development for numerous teams within their Manchester offices working towards the
                  goal of creating their car-hire platform integration with their client, RyanAir.
                  They required help to meet a deadline in which my role was to help develop and
                  integrate a search facility as part of RyanAir&apos;s user journey.
                </p>

                <p>
                  Booking.com provide RyanAir&apos;s Car Hire stage of the user experience. My role
                  was to support and aid the development of the UI that facilitates the search and
                  selection of hire cars and help integrate that into the API provided by RyanAir.
                </p>

                <p>
                  This project was the first time I had worked utilising Mob Programming methodology
                  which I found to be an excellent way of working, the code quality was outstanding
                  as a result of having many people suggesting alternative approaches to solving
                  problems which contributed to the final product being of an extremely high
                  standard.
                </p>

                <p>
                  The search and sort facility we produced was comprised of as a series of
                  composable and reusable modular components, including a &apos;snake&apos;
                  calendar, search facility all written in ES6 Javascript and React. We succeeded in
                  delivering the project on-time despite the deadline being considered by some to be
                  difficult-to-impossible, both booking.com and RyanAir were delighted with this
                  achievement.
                </p>
              </div>
              <div className={styles.imagesContainer}>
                <LazyLightbox src={booking1} thumbSrc={booking1Thumb} />
                <LazyLightbox src={booking2} thumbSrc={booking2Thumb} />
                <LazyLightbox src={booking3} thumbSrc={booking3Thumb} />
                <LazyLightbox src={booking4} thumbSrc={booking4Thumb} />
                <LazyLightbox src={booking0} thumbSrc={booking0Thumb} />
                <LazyLightbox src={booking6} thumbSrc={booking6Thumb} />
                <LazyLightbox src={booking7} thumbSrc={booking7Thumb} />
                <LazyLightbox src={booking8} thumbSrc={booking8Thumb} />
                <LazyLightbox src={booking9} thumbSrc={booking9Thumb} />
                <LazyLightbox src={booking10} thumbSrc={booking10Thumb} />
                <LazyLightbox src={booking11} thumbSrc={booking11Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="AE Live"
            icon={['AELive']}
            fromDate={{ month: 'JULY', year: '2018' }}
            toDate={{ month: 'OCT', year: '2018' }}
            hashLink="aelive"
          >
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <h5>Live Video Titles Overlay Control Application</h5>
                <p>
                  My second client operating as Perpetual Summer Ltd was AE Live (formerly AE
                  Graphics) a company based near London who provide video overlay title services to
                  prestige international sports broadcasters such as Sky TV and BBC. I was tasked
                  with developing a browser-based video overlay interface comprised of a client
                  application to enable the user to control the information shown and an overlay
                  application utilising CasparCG to overlay information graphics over live-video for
                  a variety of sports including international cricket, tour de France and a number
                  of professional league football associations worldwide.
                </p>

                <p>
                  I was given full responsibility on this project and worked alone. I had autonomy
                  on the stack we used, I chose to use React for the overlay visual output due to
                  its speed and responsiveness which was essential due to the nature of the product
                  requirements. I chose Redux & Redux Thunks as the state engine due to the wealth
                  of resources available, at the time it was the go-to state container for react
                  developement. Both the client and overlay visuals were designed by myself and
                  animated using SCSS (no external libraries to keep bundle size to a minimum) which
                  I used to produce instantaneous transitions and flourishes.
                </p>

                <p>
                  I was responsible for reporting progress back to the project management team and
                  board where I presented regular updates to keep them updated with progress and get
                  regular feedback and direction on project goals. This was extremely interesting
                  and challenging work only tangentially linked to work I had done before, I enjoyed
                  the challenge and was extremely proud of the product that was delivered.
                </p>
              </div>
              <div className={styles.singleColImagesContainer}>
                <Video videoArray={aeVideoArray} />
                <LazyLightbox src={ae1} thumbSrc={ae1Thumb} />
                <LazyLightbox src={ae2} thumbSrc={ae2Thumb} />
                <LazyLightbox src={ae3} thumbSrc={ae3Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Degree 53 &amp; Betfred"
            icon={['degree53', 'Betfred']}
            fromDate={{ month: 'MAR', year: '2018' }}
            toDate={{ month: 'MAY', year: '2018' }}
            hashLink="betfred"
          >
            <div className={styles.singleColContentContainer}>
              <div className={styles.copyContainer}>
                <h5>Regulatory Compliance &amp; Maintenance</h5>
                <p>
                  I founded Perpetual Summer Ltd and started working as a contractor in spring 2018
                  and was immediately hired by degree53 to work on the Betfred.mobi online gambling
                  portal.
                </p>

                <p>
                  My duties mainly involved small works, back-end intergration, updates to existing
                  functionality, fixes for validation on registration and some design change
                  implementation on their games offerings.
                </p>

                <p>
                  Although this role was only short-term, to fill a requirements shortfall with
                  compliance deadlines looming, I enjoyed the work and my previous experience in
                  this sector enabled me to quickly intergrate and get productive.
                </p>
              </div>
              <div className={styles.singleColImagesContainer}>
                <LazyLightbox src={betfredImage} thumbSrc={betfredImage} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Perpetual Summer Ltd."
            icon={['PerpetualSummer']}
            fromDate={{ month: 'MAR', year: '2018' }}
            hashLink="perpetual-summer"
          >
            <h5>Contract Software Development &amp; Consultancy Services Company</h5>
            <p>
              I founded Perpetual Summer Ltd in order to provide my software development services on
              a contractual basis with a view to being able to travel with my work. Shortly after
              starting the company I gained my first client, Degree 53.{' '}
            </p>
            <p>
              I am proud to have built a business that has worked with some fantastic companies and
              some wonderful developers on projects both great and small. Perpetual Summer has hit
              the ground running, is going strong and I am excited about what the future holds.
            </p>
            <p>
              As well as providing software development services to a good variety of clients I have
              also created some Open-Source projects under the Perpetual Summer banner. More details
              of these can be found in the <Link to="/projects">Projects & Open Source</Link>{' '}
              section of this website.
            </p>
          </PortfolioItem>
        </ScrollIntoView>
        <ScrollIntoView>
          <PortfolioItem
            themeName={themeName}
            heading="Sports Information Services"
            icon={['SIS']}
            fromDate={{ month: 'AUG', year: '2014' }}
            toDate={{ month: 'FEB', year: '2018' }}
            hashLink="sis"
          >
            <h5>Sports Betting Data &amp; Video Services Provider</h5>
            <p>
              SIS provide sports betting data and video content to major bookmakers and gaming
              service providers.
            </p>
            <p>
              SIS is the official betting data partner for LaLiga, and the exclusive provider of
              live data for more than 2,500 matches a season across every game of the top three
              football leagues and the major cup competitions in Spain.
            </p>

            <h4>
              <strong>SIS Projects</strong>
            </h4>

            <h6 style={{ marginTop: 0 }}>24 Hour Greyhound Racing Title Overlay Service</h6>
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <p>
                  Due to the variance between subscriptions caused by the numerous customisations
                  possible, we needed an application to run during the break between live-race video
                  streams of Dogs races in order to prevent &apos;dead air&apos;. We wanted to
                  produce something that displayed the results of previous races and the racecard
                  for the next race. It needed to only display relevant data for the services and
                  locations available to that specific subscriber - a high degree of customisation
                  SIS were not previously able to offer.
                </p>
                <p>
                  This displays throughout the day and fills the time between live streamed events,
                  it shows the previous 3 results and the next 3 racecards and changes in realtime
                  to only show events that relate to the services paid for by the subscriber. No
                  superfluous result or racecard data will be displayed for any events that occur,
                  that were not requested as part of their service subscription. This degree of
                  bespoke channel tailoring has not been done before outside of in-house channels
                  run by operators.
                </p>
              </div>
              <div className={styles.sisRightColContainer}>
                <div className={styles.sisImagesContainer}>
                  <LazyLightbox src={greyhounds1} thumbSrc={greyhounds1Thumb} />
                  <LazyLightbox src={greyhounds2} thumbSrc={greyhounds2Thumb} />
                  <LazyLightbox src={greyhounds3} thumbSrc={greyhounds3Thumb} />
                  <LazyLightbox src={greyhounds4} thumbSrc={greyhounds4Thumb} />
                  <LazyLightbox src={greyhounds5} thumbSrc={greyhounds5Thumb} />
                  <LazyLightbox src={greyhounds6} thumbSrc={greyhounds6Thumb} />
                </div>
                <div className={styles.sisVideoContainer}>
                  <Video videoArray={sisVideoArray} />
                </div>
              </div>
            </div>

            <h6>Runner DB</h6>
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <p>
                  RunnerDB is a system that captures data of horse & dogs races including results,
                  runners, racetracks, jockeys and owners. It also features a front end interface
                  for an experimental live geo-tracking system currently being prototyped and
                  developed by SIS for instant live positional monitoring of Runners (both horses
                  and dogs) during races.
                </p>
                <p>
                  Using google maps we have overlayed geopositional data to track runners and feed
                  that information into the dynamic racecards system for live race progress data.
                  There is currently no other racing data services company in the world that has
                  this feature, its something our dev team is particularly proud of.
                </p>
              </div>
              <div className={styles.sisImagesContainer}>
                <LazyLightbox src={runnerDb1} thumbSrc={runnerDb1Thumb} />
                <LazyLightbox src={runnerDb2} thumbSrc={runnerDb2Thumb} />
                <LazyLightbox src={runnerDb3} thumbSrc={runnerDb3Thumb} />
                <LazyLightbox src={runnerDb4} thumbSrc={runnerDb4Thumb} />
                <LazyLightbox src={runnerDb5} thumbSrc={runnerDb5Thumb} />
                <LazyLightbox src={runnerDb6} thumbSrc={runnerDb6Thumb} />
              </div>
            </div>

            <h6>Streaming Access Control</h6>
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <p>
                  Streaming Access Control was designed to allow customers of SIS&apos;
                  video-over-fibre services to customise the feeds provided.
                </p>

                <p>
                  This app was conceived to allow deep customisation of streams provided by SIS,
                  including both scheduling and branded titles & wipes.
                </p>
              </div>
              <div className={styles.sisImagesContainer}>
                <LazyLightbox src={sac1} thumbSrc={sac1thumb} />
                <LazyLightbox src={sac2} thumbSrc={sac2thumb} />
                <LazyLightbox src={sac3} thumbSrc={sac3thumb} />
                <LazyLightbox src={sac4} thumbSrc={sac4thumb} />
                <LazyLightbox src={sac5} thumbSrc={sac5thumb} />
                <LazyLightbox src={sac6} thumbSrc={sac6thumb} />
              </div>
            </div>

            <h6>Dynamic Racecards</h6>
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <p>
                  Dynamic Racecards was a project designed to provide a drop-in application to be
                  used in any situation to display the days racing information. With the ability to
                  filter between horses and dogs events, by nation or region, or by individual
                  track. This was utilised in a huge variety of places from internal applications to
                  video overlays which can be seen on horseracing events broadcast from around the
                  world.
                </p>
                <p>
                  Prior to the race it displays up to the second price updates, withdrawals jockey
                  changes and more. During the race, when used in conjuntion with the In-running
                  software we developed, diplays live visual updates to display running order in the
                  live event viewed. Results and other information was displayed after the event.
                  This ran in realtime 24 hours a day and consumed data provided from around the
                  world and was a long running project which was regularly updated throughout my
                  time at SIS.
                </p>
              </div>
              <div className={styles.sisImagesContainer}>
                <LazyLightbox src={racecards1} thumbSrc={racecards1Thumb} />
                <LazyLightbox src={racecards2} thumbSrc={racecards2Thumb} />
                <LazyLightbox src={racecards3} thumbSrc={racecards3Thumb} />
                <LazyLightbox src={racecards4} thumbSrc={racecards4Thumb} />
                <LazyLightbox src={racecards5} thumbSrc={racecards5Thumb} />
                <LazyLightbox src={racecards6} thumbSrc={racecards6Thumb} />
                <LazyLightbox src={racecards7} thumbSrc={racecards7Thumb} />
                <LazyLightbox src={racecards8} thumbSrc={racecards8Thumb} />
                <LazyLightbox src={racecards9} thumbSrc={racecards9Thumb} />
                <LazyLightbox src={racecards10} thumbSrc={racecards10Thumb} />
              </div>
            </div>

            <h6>Football Services</h6>
            <div className={styles.contentContainer}>
              <div className={styles.copyContainer}>
                <p>
                  Football Services is the central portal for SIS&apos; global client base of sports
                  gambling clients, its purpose is to provide customers of SIS&apos; sports betting
                  data services the tools to best utilise the data available to them to provide
                  betting opportunities to their customers. It also enabled realtime data collection
                  from matches via the Football Trader App, utilised by SIS staff attending the
                  games who captured events in realtime using our android tablet based data
                  collection tool.
                </p>
              </div>
              <div className={styles.sisImagesContainer}>
                <LazyLightbox src={football1} thumbSrc={football1Thumb} />
                <LazyLightbox src={football2} thumbSrc={football2Thumb} />
                <LazyLightbox src={football3} thumbSrc={football3Thumb} />
                <LazyLightbox src={football4} thumbSrc={football4Thumb} />
                <LazyLightbox src={football5} thumbSrc={football5Thumb} />
              </div>
            </div>
          </PortfolioItem>
        </ScrollIntoView>
      </div>
      <div>
        <ScrollIntoView>
          <div id="archive">
            <h1 className={styles.heading}>Older Projects</h1>
            <PortfolioItem
              themeName={themeName}
              heading={
                <span>
                  TDCbitworks
                  <wbr />
                  .co.uk
                </span>
              }
              fromDate={{ month: 'SEPT', year: '2013' }}
              toDate={{ month: 'FEB', year: '2014' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    TDCbitworks.co.uk is my personal Blogging site, I developed it in order to give
                    myself a platform with which to write articles, publish them online and draw
                    attention to them using the usual social media channels such as Twitter,
                    Facebook, Google Plus, Pinterest and others. In order to best facilitate this I
                    decided to integrate it into a bespoke Content management system. It would have
                    been easier to incorporate this into a existing platform such as Wordpress or
                    even Blogger however I wanted to see if I could develop something similar myself
                    and really test my PHP and mySQL programming skills.
                  </p>

                  <p>
                    As this is a blog site I wanted the copy to be the dominant feature and not
                    overwhelm the site with superfluous features and non-relevant information. With
                    the ability to share articles via social media my goal is to update this
                    regularly and get a respectable author rank in order to better advertise my
                    talents as a developer and designer with a good background in search engine
                    marketing.
                  </p>

                  <p>
                    I developed this website to use as my blogging site where I can post articles on
                    web design, technology and any other topics that compel me to write. In order to
                    acheive that I needed a content management system, rather than use an out of the
                    box solution like wordpress, I decided to attempt to write my own cms which I
                    called Scribe CMS, more of which you can read about below.
                  </p>
                </div>
                <div className={styles.sisImagesContainer}>
                  <LazyLightbox src={tdc1} thumbSrc={tdc1thumb} />
                  <LazyLightbox src={tdc2} thumbSrc={tdc2thumb} />
                  <LazyLightbox src={tdc3} thumbSrc={tdc3thumb} />
                  <LazyLightbox src={tdc4} thumbSrc={tdc4thumb} />
                  <LazyLightbox src={tdc5} thumbSrc={tdc5thumb} />
                  <LazyLightbox src={tdc6} thumbSrc={tdc6thumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading="Scribe CMS"
              fromDate={{ month: 'SEPT', year: '2013' }}
              toDate={{ month: 'FEB', year: '2014' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    In order to create and manage articles on the TDCbitworks.co.uk site I needed a
                    content managment system. The creation of this app, that I have since named
                    Scribe CMS, was a challenging project that I thoroughly enjoyed developing.
                  </p>
                  <p>
                    My skills as a developer were improved as I have had to learn a great deal more
                    about PHP and jQuery in order to get the features and functionality required. I
                    was imensely proud of this work and it was successfully creating this fully
                    featured CMS that gave me the confidence in my skills to seriously persue a
                    career in web application development.
                  </p>
                </div>
                <div className={styles.imagesContainer}>
                  <LazyLightbox src={scribe1} thumbSrc={scribe1thumb} />
                  <LazyLightbox src={scribe2} thumbSrc={scribe2thumb} />
                  <LazyLightbox src={scribe3} thumbSrc={scribe3thumb} />
                  <LazyLightbox src={scribe4} thumbSrc={scribe4thumb} />
                  <LazyLightbox src={scribe5} thumbSrc={scribe5thumb} />
                  <LazyLightbox src={scribe6} thumbSrc={scribe6thumb} />
                  <LazyLightbox src={scribe7} thumbSrc={scribe7thumb} />
                  <LazyLightbox src={scribe8} thumbSrc={scribe8thumb} />
                  <LazyLightbox src={scribe9} thumbSrc={scribe9thumb} />
                  <LazyLightbox src={scribe10} thumbSrc={scribe10thumb} />
                  <LazyLightbox src={scribe11} thumbSrc={scribe11thumb} />
                  <LazyLightbox src={scribe12} thumbSrc={scribe12thumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading={
                <span>
                  Microcosmic
                  <wbr />
                  .co.uk
                </span>
              }
              fromDate={{ month: 'FEB', year: '2012' }}
              toDate={{ month: 'SEPT', year: '2013' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    Microcosmic is the front-of-shop services website to promote the web hosting
                    services I can provide on a freelance basis. I wanted to make it bright and
                    vibrant yet still clear and uncluttered. The responsive layout enables the quick
                    addition of content without the need for additional styling to look good on
                    mobile and tablet browsers.
                  </p>

                  <p>
                    Based on a 12 Column Grid system, this layout is designed to degrade and
                    reorganise the content to best suit the device it is being viewed upon.
                    Responsive web design has become a must-have skill in web development and the
                    Microcosmic site demonstrates my abilities in this field.
                  </p>
                </div>
                <div className={styles.sisImagesContainer}>
                  <LazyLightbox src={microcosmic1} thumbSrc={microcosmic1Thumb} />
                  <LazyLightbox src={microcosmic2} thumbSrc={microcosmic2Thumb} />
                  <LazyLightbox src={microcosmic3} thumbSrc={microcosmic3Thumb} />
                  <LazyLightbox src={microcosmic4} thumbSrc={microcosmic4Thumb} />
                  <LazyLightbox src={microcosmic5} thumbSrc={microcosmic5Thumb} />
                  <LazyLightbox src={microcosmic6} thumbSrc={microcosmic6Thumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading="The Needles Park"
              fromDate={{ month: 'MAY', year: '2014' }}
              toDate={{ month: 'JUNE', year: '2014' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    The Needles Park is a Tourist Attraction situated at Alum Bay on the Isle of
                    Wight. With a range of attractions including a fun fair, 4D Cinema and Chairlift
                    down to the famous sands of Alum Bay, The Needles Park required a website that
                    illustrated the natural beauty of the area but also promoted the park facilities
                    and entertainment as well as highlighting the rich history of the area.
                  </p>

                  <p>
                    Featuring a Blog, Image gallery, History section and Information on each
                    attraction, this website proved to be a large undertaking. The fully responsive
                    design is based on bootstrap and also features a custom made &apos;sticky&apos;
                    menu to enable the user to navigate the site at all times on tablets and mobile
                    handsets.
                  </p>

                  <p>
                    This website was built by myself while under contract at cq2. I thoroughly
                    enjoyed working on this project, the source material was excellent and this was
                    reflected in the results, which the client was delighted with.
                  </p>
                </div>
                <div className={styles.sisImagesContainer}>
                  <LazyLightbox src={needles1} thumbSrc={needles1Thumb} />
                  <LazyLightbox src={needles2} thumbSrc={needles2Thumb} />
                  <LazyLightbox src={needles3} thumbSrc={needles3Thumb} />
                  <LazyLightbox src={needles4} thumbSrc={needles4Thumb} />
                  <LazyLightbox src={needles5} thumbSrc={needles5Thumb} />
                  <LazyLightbox src={needles6} thumbSrc={needles6Thumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading="Snowdon Mountain Railway"
              fromDate={{ month: 'APR', year: '2014' }}
              toDate={{ month: 'MAY', year: '2014' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    Based on a 12 Column Grid system, this layout is designed to degrade and
                    reorganise the content to best suit the device it is being viewed upon.
                    Responsive web design has become a must-have skill in web development and the
                    Microcosmic site demonstrates my abilities in this field.
                  </p>

                  <p>
                    Snowdon Mountain Railway is a tourist attraction situated at the peak of Mount
                    Snowdon in Snowdonia, Wales. It consists of a rack and pinion railway line that
                    runs up the mountainside and stops at a visitor centre at the peak of the
                    mountain some 3’560 ft up. The railway service features both steam and diesel
                    engines which run daily, weather permitting.
                  </p>

                  <p>
                    The client required a website that provides all relevant information about the
                    attraction including train travel times, mountain weather conditions, journey
                    prices as well as technical and historical information about the trains
                    themselves. The site is fully responsive and comes complete with a content
                    management system to enable the client to edit and update the sites’ content,
                    news and articles and add more pages going forward as required.
                  </p>
                </div>
                <div className={styles.responsiveImageContainer}>
                  <LazyLightbox src={snowdon1} thumbSrc={snowdon1Thumb} />
                  <LazyLightbox src={snowdon2} thumbSrc={snowdon2Thumb} />
                  <LazyLightbox src={snowdon3} thumbSrc={snowdon3Thumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading="Skills Solutions"
              fromDate={{ month: 'FEB', year: '2014' }}
              toDate={{ month: 'MAR', year: '2014' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    Skills Solutions provide Apprenticeships and Higher Education courses for Teens
                    and Young Adults in and around the Manchester area. We were tasked with
                    providing a website that provides information on their range of courses and
                    services and a course application facility.
                  </p>
                </div>
                <div className={styles.skillsImageContainer}>
                  <LazyLightbox src={skills1} thumbSrc={skills1Thumb} />
                  <LazyLightbox src={skills2} thumbSrc={skills2Thumb} />
                  <LazyLightbox src={skills3} thumbSrc={skills3Thumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading="Pension Claims Expert"
              fromDate={{ month: 'DEC', year: '2013' }}
              toDate={{ month: 'JAN', year: '2014' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    This website was designed for the same client as Enhanced Annuities Quotes. The
                    specification was much the same, a simple single page website with the focus and
                    emphasis on conversion. The site provides the basic information required but is
                    geared towards calling the user to action in order to harvest contact
                    information.
                  </p>
                </div>
                <div className={styles.inlineImageContainer}>
                  <LazyLightbox src={pension1} thumbSrc={pension1Thumb} />
                  <LazyLightbox src={pension2} thumbSrc={pension2Thumb} />
                  <LazyLightbox src={pension3} thumbSrc={pension3Thumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading="Enhanced Annuities Quotes"
              fromDate={{ month: 'SEPT', year: '2013' }}
              toDate={{ month: 'NOV', year: '2013' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    This one-page conversion focused website was designed with simplicity in mind.
                    Foremost in the clients mind was the potential target audience are most likely
                    to be elderly people, potentially with limited knowledge on using websites.
                  </p>
                  <p>
                    This solution enabled us to always keep the focus on engaging the visitor,
                    providing essential information and encouraging them to apply via a contact form
                    which would prompt a follow up call, which was the ultimate goal of the site. As
                    they were pleased with the results, this client has since utilised my services
                    with many other projects.
                  </p>
                </div>
                <div className={styles.singleImageContainer}>
                  <LazyLightbox src={enhanced} thumbSrc={enhancedThumb} />
                </div>
              </div>
            </PortfolioItem>
            <PortfolioItem
              themeName={themeName}
              heading="Tony Donelan Woodcraft Services"
              fromDate={{ month: 'APR', year: '2013' }}
              toDate={{ month: 'MAY', year: '2013' }}
            >
              <div className={styles.contentContainer}>
                <div className={styles.copyContainer}>
                  <p>
                    I was approached by a former colleague who required a website for an
                    acquaintance. Tony Donelan Woodcraft are a company that specialise in hand
                    crafted garden furniture operating from their premises in Belfast, Ireland.
                  </p>
                  <p>
                    The design for the site had been set prior to my involvement so all that was
                    required was to develop the site based on the PSD mockups provided, I managed to
                    get this completed in just a few hours, this illustrates my ability to deliver
                    projects even under tight deadlines.
                  </p>
                </div>
                <div className={styles.singleImageContainer}>
                  <LazyLightbox src={donelan} thumbSrc={donelanThumb} />
                </div>
              </div>
            </PortfolioItem>
          </div>
        </ScrollIntoView>
      </div>
    </div>
  )
}
Portfolio.propTypes = {
  themeName: propTypes.string.isRequired,
}
export default Portfolio
