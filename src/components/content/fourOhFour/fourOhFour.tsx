import React from 'react'
import propTypes from 'prop-types'
import classNames from 'classnames/bind'
import { Link } from 'react-router-dom'
import { THEME } from '../../../constants/theme'

import { ThemeNameType } from '../../../Types'

import styles from './fourOhFour.module'
let cx = classNames.bind(styles)

const FourOhFour = ({ themeName }: ThemeNameType) : JSX.Element => {
  let containerClasses = cx({
    container: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_sprawl: themeName === THEME.SPRAWL,
  })

  return (
    <div className={containerClasses}>
      <h1 title="404" className={styles.glitchText}>
        404
      </h1>
      <h5 className={styles.pageNotFound}>Page Not Found</h5>
      <h6 className={styles.linkText}>
        <Link to="/">HOME</Link> &bull; <Link to="/portfolio">PORTFOLIO</Link> &bull;{' '}
        <Link to="/projects">PROJECTS</Link> &bull; <Link to="/resume">RESUME</Link> &bull;{' '}
        <Link to="/contact">CONTACT</Link>
      </h6>
    </div>
  )
}

FourOhFour.propTypes = {
  themeName: propTypes.string.isRequired,
}

export default FourOhFour
