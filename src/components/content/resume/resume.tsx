import React from 'react'
import propTypes from 'prop-types'
import classNames from 'classnames/bind'
import ScrollIntoView from '../../common/scrollIntoView/scrollIntoView'
// @ts-ignore
import MultiButton from '@perpetualsummer/catalyst-elements/MultiButton'
import { THEME } from '../../../constants/theme'

import { ThemeNameType } from '../../../Types'

import styles from './resume.module'
let cx = classNames.bind(styles)
// import CVpdf from '../../../public/files/AnthonyCregan_CV.pdf'
// import CVdocx from '../../../public/files/AnthonyCregan_CV.docx'
// import CVrtf from '../../../public/files/AnthonyCregan_CV.rtf'

const Resume = ({ themeName }: ThemeNameType): JSX.Element => {
  const downloadOptions = [
    {
      label: 'Download DOCX',
      action: () =>
        downloadFile('https://www.anthonycregan.co.uk/public/files/AnthonyCregan_CV.docx'),
    },
    {
      label: 'Download PDF',
      action: () =>
        downloadFile('https://www.anthonycregan.co.uk/public/files/AnthonyCregan_CV.pdf'),
    },
    {
      label: 'Download RTF',
      action: () =>
        downloadFile('https://www.anthonycregan.co.uk/public/files/AnthonyCregan_CV.rtf'),
    },
  ]

  const downloadFile = (file:string) => {
    const link = document.createElement('a')
    link.href = file
    link.download = file.split('/').pop()
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }

  let containerClasses = cx({
    viewContainer: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_sprawl: themeName === THEME.SPRAWL,
    theme_haas: themeName === THEME.HAAS,
  })

  return (
    <div className={containerClasses}>
      <MultiButton
        className={styles.downloadButton}
        options={downloadOptions}
        id="downloadCVbutton"
      />

      <ScrollIntoView>
        <div className={styles.pageWrapper} id="page1">
          <div className={styles.pageContent}>
            <h1 className={styles.center}>Anthony Cregan - Curriculum Vitae</h1>
            <div className={styles.headerInfo}>
              <div className={styles.leftCol}>
                <p>
                  email: <a href="mailto:hello@anthonycregan.co.uk">hello@anthonycregan.co.uk</a>
                </p>
                <p>
                  tel: <a href="tel:6596133184">+65 96133184</a>
                </p>
                <p>
                  portfolio:{' '}
                  <a href="https://www.anthonycregan.co.uk/" target="_blank" rel="noreferrer">
                    anthonycregan.co.uk
                  </a>
                </p>
              </div>
              <div className={styles.rightCol}>
                <p>River Valley</p>
                <p>Singapore</p>
              </div>
            </div>
            <h2>Personal</h2>
            <p>
              I am a talented, ambitious and self-motivated web designer with a strong technical
              background who possesses self-discipline and the ability to work with the minimum of
              supervision. I am able to play a key role throughout a sites development to ensure
              cross-browser compatibility, accessibility and an enjoyable customer experience with
              usability always at the forefront of my design considerations.
            </p>
            <p>
              I thrive on developing engaging creative solutions, I can work to a deadline and I
              have the ability to produce detailed technical specifications from discussions with
              client/project managers to understand their requirements and produce solutions that
              are as visually impressive as they are easy to use.
            </p>
            <p>
              I consider myself highly-adaptable, I can absorb new ideas and can communicate clearly
              and effectively, this gives me the ability to quickly interpret requirements and in
              turn, provide great looking web applications and websites that often exceed
              expectation.
            </p>

            <h2>Key Skills &amp; Competencies</h2>
            <ul>
              <li>PC and Mac Literate.</li>
              <li>
                Proven and demonstrable experience of HTML5, ES6 Javascript, JSX, React.js, SASS,
                CSS3, jQuery, PHP and mySQL to create beautiful and functional web solutions.
              </li>
              <li>
                Confident using modern, cutting-edge build tooling for react front end development.
              </li>
              <li>
                Years of experience working in the web industry in a business-orientated
                environment.
              </li>
              <li>
                My design philosophy is a User Experience and User Interaction first approach.
              </li>
              <li>Experience of working to a tight deadline.</li>
              <li>Great interpersonal skills from years of working in client contact roles.</li>
            </ul>

            <h2>Employment History</h2>

            <h3>
              <strong>Aleph Labs</strong>
            </h3>
            <h4>Senior Application Developer</h4>
            <h5>August 2021 - Present</h5>
            <h6>Full Time Employment</h6>
            <p>
              I was recruited by Aleph-Labs to bring my React.js and Redux experience to help with a
              migration project for their client Standard Chartered Bank. Working with an existing
              team of talented developers we are tasked with migrating their existing online retail
              banking application from Ember to React.
            </p>
            <p>
              This project involves splitting the existing UI into logical modular components and
              styling them to match the existing company design. Reverse engineering the existing
              Ember application has been an interesting new challenge as I have never previously
              worked with that particular framework. This primarily involves deriving the
              functionality of the existing application and writing new, equivelent modules in
              React.js then composing the seperate components together using Webpack&apos;s Module
              Federation features.
            </p>

            <h3>
              <strong>United Overseas Bank</strong>
            </h3>
            <h4>Senior Application Developer</h4>
            <h5>June 2020 - August 2021</h5>
            <h6>Full Time Employment</h6>
            <p>
              In early 2020 I moved to Singapore and was hired by UOB in the role of senior
              applications developer working on their online-banking portal, UOB Infinity. The scope
              of this role was to develop the Hong Kong variant of their core banking application.
            </p>
            <p>
              This involved identifying, planning and implementing the changes required to modify
              the base application, which was designed for the Singapore market and adapt it to
              accommodate the differences in the Hong Kong market. This includes changes required to
              currency, forex, transaction types and banking facilities that are unique or specific
              to the Hong Kong banking sector.
            </p>
            <p>
              Due to the breadth of changes required across all areas of the application I gained a
              great deal of experience as we were not focused on one specific area of the app. I
              also took responsibility for adapting and expanding their core UI component set to
              facilitate the additional features that were required to accommodate the Hong Kong
              variant.
            </p>
          </div>
        </div>
      </ScrollIntoView>
      <ScrollIntoView>
        <div className={styles.pageWrapper} id="page2">
          <div className={styles.pageContent}>
            <p>
              <sup>
                <strong>
                  <u>United Overseas Bank</u>
                </strong>{' '}
                Continued
              </sup>
            </p>
            <p>
              Having previously had no experience in the banking sector I have found the work at UOB
              rewarding and enjoyed the challenge of having to understand the obstacles unique to
              the finance industry and how we can develop software and user interface solutions to
              overcome them.
            </p>

            <h3>
              <strong>Booking.com</strong>
            </h3>
            <h4>React Development Consultant</h4>
            <h5>July 2019 - December 2019</h5>
            <h6>Contract - Perpetual Summer Ltd Client</h6>
            <p>
              I was approached by Booking.com who were looking for people with React experience to
              work on an integration project for their client, RyanAir. Our breif was to develop UI
              components to facilitate the selection and booking of hire-cars as part of the RyanAir
              flight booking user journey.
            </p>
            <p>
              I worked across a number of teams providing API interfaces and front-end components to
              facilitate the search and selection of hire cars at airports throughout the world via
              RyanAirs website.
            </p>
            <p>
              We had a very tight deadline to deliver to RyanAir but we managed to complete the
              project on time and both Booking.com and RyanAir commended the work we delivered.
            </p>

            <h3>
              <strong>AE Live</strong>
            </h3>
            <h4>Application Development Consultant</h4>
            <h5>July 2018 - October 2018</h5>
            <h6>Contract - Perpetual Summer Ltd Client</h6>
            <p>
              I was engaged by a company based near London who provide video overlay title services
              to international sports broadcasters such as Sky TV, BBC Sport and Bein Sports.
            </p>
            <p>
              I was tasked with developing a browser-based video overlay service comprised of a
              client application to enable the user to control the information shown and an overlay
              application utilising CasparCG to overlay information graphics over live-video for a
              variety of sports including international cricket, tour de France and a number of
              professional league football associations worldwide.
            </p>

            <h3>
              <strong>Degree53 / BetFred</strong>
            </h3>
            <h4>React Development Consultant</h4>
            <h5>March 2018 - May 2018</h5>
            <h6>Contract - Perpetual Summer Ltd Client</h6>
            <p>
              Shortly after founding Perpetual Summer Ltd I was immediately contracted upon initial
              discussions with degree53, a gaming software development company who needed somebody
              with experience in the gaming industry and who can develop in react.js.
            </p>
            <p>
              This was a short-term contract and involved only small works and additional support to
              meet a compliance deadline. We worked on a react native client app for their client,
              Betfred.
            </p>

            <h3>
              <strong>Sports Information Services</strong>
            </h3>
            <h4>UI / UX Designer & Developer</h4>
            <h5>August 2014 - February 2018</h5>
            <h6>Full Time Employment</h6>
            <p>
              My Initial role at SIS Betting was HTML4 Visual Developer and my duties were to design
              and build the front end of their betting and sports data systems. After 18 months in
              this role I was promoted to UI/UX developer to better reflect the additional
              responsibilities I had undertaken in this time.{' '}
            </p>

            <p>
              Working mainly in React.js, I was tasked with developing the user experience and
              building a consistently themed user interface that delivers that user experience.
              Working alongside the Backend Team who work mainly in Java, I was responsible for
              writing the Javascript, HTML and CSS to present the back-end data in a logical,
              intuitive and easily digestible way and enable the user to manipulate that data to
              achieve their goals.{' '}
            </p>

            <p>
              As live sports betting data is subject to constant change this can be challenging and
              ultimately very rewarding work. In June 2016 I represented SIS at the React.js
              conference in Paris, France and I also provided mentoring to two graduates of Salford
              University who were brought in as part of a work placement programme.
            </p>
          </div>
        </div>
      </ScrollIntoView>
      <ScrollIntoView>
        <div className={styles.pageWrapper} id="page3">
          <div className={styles.pageContent}>
            <p>
              <sup>
                <strong>
                  <u>Employment History</u>
                </strong>{' '}
                Continued
              </sup>
            </p>

            <h3>
              <strong>CQ2</strong>
            </h3>
            <h4>Web Developer</h4>
            <h5>April 2014 - August 2014</h5>
            <h6>Full Time Employment</h6>
            <p>
              I was contracted to work as a front-end web developer at CQ2, a full-service design
              agency based in Old Trafford. My duties included the development of front-end user
              interfaces for commercial web sites and associated software such as web apps, mobile
              apps and bespoke content management systems.
            </p>
            <p>
              I also took on a great deal of project management responsibility in this role, the
              experience gained has proven most useful and enlightening. These additional
              responsibilities included client liaison and identifying and addressing project
              requirements.
            </p>

            <h3>
              <strong>Microcosmic Internet Solutions</strong>
            </h3>
            <h4>Web Designer & Developer</h4>
            <h5>November 2013 - April 2014</h5>
            <h6>Self-Employment</h6>
            <p>
              Microcosmic Internet was my first attempt at self-employment. It was a freelance Web
              Design, Web Development and Online Marketing services company.
            </p>
            <p>
              I only gathered a handful of clients but they were, without exception, delighted with
              the work delivered. This experience gave me the confidence to establish my current
              software development consultancy company, Perpetual Summer Ltd.
            </p>

            <h3>
              <strong>First Found</strong>
            </h3>
            <h4>SEO Consultant & Adwords Account Manager</h4>
            <h5>June 2009 - November 2013</h5>
            <h6>Full Time Employment</h6>
            <p>
              My duty as SEO consultant was to manage SEO accounts, update clients’ sites to meet
              SEO best practices, liaise with them and ultimately provide good search engine ranking
              results for relevant, high search volume phrases.
            </p>
            <p>
              I was only with the company a short time before being asked to adopt additional duties
              as a PPC account manager. This involved creating adwords campaigns for a broad variety
              of clients from basic local services sites to full international e-commerce sites.
            </p>
            <p>
              My client base was broad and varied. I managed SEO campaigns for well-known
              international companies such as Ariston, Litron Lasers and Vital Services as well as
              many smaller local services and retail sites.
            </p>
            <p>
              During my time I managed over 400 accounts and the exposure to various technologies
              and frameworks I encountered on such a broad range of clients website has proven
              invaluable in helping me develop and improve as a Web Designer and Developer.
            </p>

            <h2>Other Jobs</h2>
            <h4>
              <strong>Ritz Recruitment</strong>
            </h4>
            <h5>Administration and HR</h5>
            <h6>August 2007 - June 2009</h6>

            <h4>
              <strong>Mercedes Benz</strong>
            </h4>
            <h5>Customer Liaison Executive</h5>
            <h6>February 2006 - August 2007</h6>

            <h4>
              <strong>J.D Williams & Co</strong>
            </h4>
            <h5>Customer Service Advisor</h5>
            <h6>October 2002 - April 2005</h6>

            <h4>
              <strong>SY Electronics</strong>
            </h4>
            <h5>Electronics Engineer</h5>
            <h6>June 2000 - May 2002</h6>
          </div>
        </div>
      </ScrollIntoView>
      <ScrollIntoView>
        <div className={styles.pageWrapper} id="page4">
          <div className={styles.pageContent}>
            <h2>Open Source Projects</h2>
            <h4>
              <strong>Catalyst Elements</strong>
            </h4>
            <h5>React Layout, Utility and UI Component Library</h5>
            <p>
              A UI library for use with React-Catalyst. It provides modular react components for
              Layout, Input, Data Visualisation and a handful of utilities and hooks.
              <br />
              <a
                href="https://catalyst-elements.perpetualsummer.ltd/"
                target="_blank"
                rel="noreferrer"
              >
                https://catalyst-elements.perpetualsummer.ltd/
              </a>
              <br />
              Also available on{' '}
              <a
                href="https://www.npmjs.com/package/@perpetualsummer/catalyst-elements"
                target="_blank"
                rel="noreferrer"
              >
                NPM
              </a>
            </p>
            <h4>
              <strong>React Catalyst</strong>
            </h4>
            <h5>React Development Environment</h5>
            <p>
              A UI library for use with React-Catalyst. It provides modular react components for
              Layout, Input, Data Visualisation and a handful of utilities and hooks.
              <br />
              <a
                href="https://react-catalyst.perpetualsummer.ltd/"
                target="_blank"
                rel="noreferrer"
              >
                https://react-catalyst.perpetualsummer.ltd/
              </a>
              <br />
              Available on{' '}
              <a
                href="https://bitbucket.org/c0un7z3r0/react-catalyst-v2/src/master/"
                target="_blank"
                rel="noreferrer"
              >
                BitBucket
              </a>
            </p>

            <h2>Academic Qualifications and Certificates</h2>
            <h4>IDEA - Innovation in Digital & Electronic Arts</h4>
            <h6>April 2003 - June 2003</h6>
            <ul>
              <li>3D Modelling Course</li>
              <li>3D Studio Max - Basic Modelling & Animation</li>
            </ul>

            <h4>E-Campus - University of Manchester</h4>
            <h6>June 2002 - September 2002</h6>
            <ul>
              <li>Practical Web Design and Development</li>
              <li>Web Design Fundamentals</li>
              <ul>
                <li>HTML & CSS</li>
                <li>Javascript</li>
                <li>Typography</li>
                <li>Dreamweaver</li>
                <li>Photoshop</li>
                <li>Flash</li>
                <li>Hosting, Domains and FTP</li>
              </ul>
            </ul>

            <h4>Tech Connect</h4>
            <h6>June 1999 - July 1999</h6>
            <ul>
              <li>Microsoft Certified Professional - Nework Administration</li>
              <li>Server and Network Administration</li>
            </ul>

            <h4>SITEC Training</h4>
            <h6>June 1997 - September 1999</h6>
            <ul>
              <li>NVQ Level 2 - PC Hardware and Support</li>
              <li>PC Diagnostics</li>
              <li>Repair and Maintenance</li>
              <li>PC Components</li>
            </ul>

            <h4>Xaverian College, Manchester</h4>
            <h6>August 1995 - June 1996</h6>
            <p>
              <strong>A-Level:</strong>
            </p>
            <ul>
              <li>English</li>
              <li>Mathematics</li>
              <li>Information Technology</li>
            </ul>

            <h4>St Vincent De Paul RC High School, Manchester</h4>
            <h6>August 1990 - June 1995</h6>
            <p>
              <strong>GCSE:</strong>
            </p>
            <ul>
              <li>English</li>
              <li>Mathematics</li>
              <li>Science</li>
              <li>Information Technology</li>
              <li>History</li>
            </ul>
            <br />
            <h6 className={styles.center}>References are available upon request</h6>
          </div>
        </div>
      </ScrollIntoView>
    </div>
  )
}
Resume.propTypes = {
  themeName: propTypes.string.isRequired,
}
export default Resume
