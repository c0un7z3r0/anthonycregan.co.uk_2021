import React, { useState, useContext, useRef } from 'react'
import classNames from 'classnames/bind'
// @ts-ignore
import InputSelect from '@perpetualsummer/catalyst-elements/InputSelect'
import SvgIcon from '../svgIcon/svgIcon'
import { THEME } from '../../../constants/theme'
import { ThemeContext } from '../../../context/ThemeContextProvider'
import { useOnClickOutside } from '../../../hooks/useOnClickOutside'
import styles from './themeSelector.module'
let cx = classNames.bind(styles)

const ThemeSelector = () : JSX.Element => {
  const [visible, setVisible] = useState(false)
  const { setThemeName, themeName, setHasRandomTheme } = useContext(ThemeContext)

  const options = [
    { value: THEME.ARTDECO, text: 'Art Deco' },
    { value: THEME.ORIGINAL, text: 'So 2016' },
    { value: THEME.SANTACRUZ, text: 'Santa Cruz' },
    { value: THEME.SPRAWL, text: 'Sprawl' },
    { value: THEME.HAAS, text: 'Haas Music' },
  ]

  const customStyles = {
    containerStyles: {
      borderColor: '#EE0000',
    },
  }

  //Close if click outside
  const settingsRef = useRef()
  useOnClickOutside(settingsRef, () => {
    if (visible) {
      setVisible(false)
    }
  })

  const changeTheme = (newThemeName: string) => {
    setHasRandomTheme(false)
    setThemeName(newThemeName)
  }

  let containerClasses = cx({
    themeSelector: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_sprawl: themeName === THEME.SPRAWL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_haas: themeName === THEME.HAAS,
  })

  return (
    <div className={containerClasses} ref={settingsRef}>
      <div className={styles.toggleButton} onClick={() => setVisible(!visible)}>
        <SvgIcon image="Gear" />
      </div>
      {visible && (
        <div className={styles.optionsContainer}>
          <InputSelect
            value={themeName}
            label="Select Theme"
            id="ThemeSelect"
            options={options}
            size={50}
            theme="rounded"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              changeTheme(e.target.value)
            }}
            style={customStyles}
          />
        </div>
      )}
    </div>
  )
}

export default ThemeSelector
