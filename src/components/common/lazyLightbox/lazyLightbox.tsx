import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
// @ts-ignore
import Lightbox from '@perpetualsummer/catalyst-elements/Lightbox'
import styles from './lazyLightbox.module'

type LazyLightboxProps = {
  thumbSrc: string,
  src: string,
  // All other props
  [x:string]: any;
}

const LazyLightbox = ({ thumbSrc, src, ...imageProps }: LazyLightboxProps): JSX.Element => {
  const [shouldLoad, setShouldLoad] = useState(false)
  const [preloadedImage, setPreloadedImage] = useState(null)
  const [imageLoaded, setImageLoaded] = useState(false)
  const [imageLoadFailed, setImageLoadFailed] = useState(false)
  const placeholderRef = useRef(null)
  const counterRef = useRef(1)

  useEffect(() => {
    let counterTimeout
    if (shouldLoad) {
      // Preloads image
      // Declare an image instance
      const preloadImage = new Image()
      // Pass the src prop to the image class source in a function...
      const loadImage = () => {
        preloadImage.src = src
      }
      // ... call that function
      loadImage()
      // If it loads successfully set the image in 'preloadedImage' state
      // and set the image loaded to true
      preloadImage.onload = () => {
        setPreloadedImage(preloadImage)
        setImageLoaded(true)
      }
      // If it fails to load...
      preloadImage.onerror = () => {
        console.log(counterRef.current)
        // if the retry count is less than 20
        if (counterRef.current <= 20) {
          // ... increment the retry counter
          counterRef.current++
          // wait half a second then try again...
          counterTimeout = setTimeout(() => {
            loadImage()
          }, 500)
        } else {
          // If its tried 20 times then its failed and we should stop retrying
          setImageLoadFailed(true)
        }
      }
    }
    return clearTimeout(counterTimeout)
  }, [shouldLoad, src])

  useEffect(() => {
    if (!shouldLoad && placeholderRef.current) {
      const observer = new IntersectionObserver(
        ([{ intersectionRatio }]) => {
          if (intersectionRatio > 0) {
            setShouldLoad(true)
          }
        },
        { rootMargin: '500px 0px 500px 0px' }
      )
      observer.observe(placeholderRef.current)
      return () => observer.disconnect()
    }
  }, [shouldLoad, placeholderRef])

  if (imageLoadFailed) {
    return <p>ERROR</p>
  }

  return shouldLoad && imageLoaded ? (
    <Lightbox image={preloadedImage.src}>
      <img {...imageProps} src={thumbSrc} />
    </Lightbox>
  ) : (
    <div ref={placeholderRef} className={styles.spinnerContainer}>
      <div className={styles.spinner} />
      <img {...imageProps} src={thumbSrc} />
    </div>
  )
}

LazyLightbox.propTypes = {
  thumbSrc: PropTypes.string,
  src: PropTypes.string,
}

export default LazyLightbox
