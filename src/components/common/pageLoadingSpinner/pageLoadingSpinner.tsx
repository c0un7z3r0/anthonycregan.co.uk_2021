import React from 'react'
import PropTypes from 'prop-types'

// @ts-ignore
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
// @ts-ignore
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
// @ts-ignore
import LoadingSpinner from '@perpetualsummer/catalyst-elements/LoadingSpinner'

type LoadingSpinnerProps = {
  height?: string
}

const PageLoadingSpinner = ({ height = '100%' }: LoadingSpinnerProps) : JSX.Element => {
  return (
    <FlexContainer
      justifyContent="center"
      alignItems="center"
      alignContent="center"
      direction="col"
      style={{ height: height }}
    >
      <FlexCell justifyContent="center">
        <LoadingSpinner />
      </FlexCell>
    </FlexContainer>
  )
}
PageLoadingSpinner.propTypes = {
  height: PropTypes.string,
}

export default PageLoadingSpinner
