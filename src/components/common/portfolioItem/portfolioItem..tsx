import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'
import SvgIcon from '../svgIcon/svgIcon'
import { THEME } from '../../../constants/theme'

import { SVGiconListTypes } from '../svgIcon/svgIcon'

import styles from './portfolioItem.module'
let cx = classNames.bind(styles)

type TimelineDateObject = {
  month: string,
  year: string
}

type PortfolioItemProps = {
  themeName: string,
  heading: string | JSX.Element,
  icon?: SVGiconListTypes[],
  fromDate: TimelineDateObject,
  toDate?: TimelineDateObject,
  children: JSX.Element | JSX.Element[],
  hashLink?: string,
}

const PortfolioItem = ({ themeName, heading, icon, fromDate, toDate, children, hashLink }: PortfolioItemProps) : JSX.Element => {
  let containerClasses = cx({
    portfolioItemContainer: true,
    theme_artDeco: themeName === THEME.ARTDECO,
    theme_original: themeName === THEME.ORIGINAL,
    theme_sprawl: themeName === THEME.SPRAWL,
    theme_santaCruz: themeName === THEME.SANTACRUZ,
    theme_haas: themeName === THEME.HAAS,
  })

  return (
    <div className={containerClasses} id={hashLink}>
      <div className={styles.timelineContainer}>
        <div className={styles.timeline}></div>
        {toDate && (
          <div className={styles.toContainer}>
            <div className={styles.timelinePointContainer}>
              <div className={styles.timelinePoint}></div>
            </div>
            <div className={styles.timelineTextContainer}>
              <div className={styles.toDateYear}>{toDate.year}</div>
              <div className={styles.toDateMonth}>{toDate.month}</div>
            </div>
          </div>
        )}
        <div className={!toDate ? styles.toContainer : styles.fromContainer}>
          <div className={styles.timelinePointContainer}>
            <div className={styles.timelinePoint}></div>
          </div>
          <div className={styles.timelineTextContainer}>
            <div className={styles.fromDateYear}>{fromDate.year}</div>
            <div className={styles.fromDateMonth}>{fromDate.month}</div>
          </div>
        </div>
      </div>

      <div className={styles.headingWithIcon}>
        <h4 className={styles.heading}>{heading}</h4>
        <div className={styles.iconContainer}>
          {icon && icon.map((iconItem) => <SvgIcon key={iconItem} image={iconItem} />)}
        </div>
      </div>
      {children}
      <hr />
    </div>
  )
}

PortfolioItem.propTypes = {
  themeName: PropTypes.string.isRequired,
  heading: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
  icon: PropTypes.arrayOf(PropTypes.string),
  fromDate: PropTypes.shape({ month: PropTypes.string, year: PropTypes.string }).isRequired,
  toDate: PropTypes.shape({ month: PropTypes.string, year: PropTypes.string }),
  children: PropTypes.node.isRequired,
  hashLink: PropTypes.string,
}

export default PortfolioItem
