import propTypes from 'prop-types'
import { useLocation } from 'react-router-dom'
import smoothScrollIntoViewIfNeeded from 'smooth-scroll-into-view-if-needed'

/**
 * Component renders a scrollIntoView container, the following will scroll into view if a hashlink is clicked (<a href="/ThisPagePath#Test">TEST</a>).
 *
 * @component
 * @example
 * return (
 *   <ScrollIntoView>
 *     <p id="Test">
 *       This is some content
 *     </p>
 *   </ScrollIntoView>
 * )
 * @param {node} children Content that the user wishes to scroll into view
 */

type ScrollIntoViewProps = {
  children: JSX.Element
}

const ScrollIntoView = ({ children }:ScrollIntoViewProps): JSX.Element => {
  const location = useLocation()
  if (location.hash !== '') {
    const element = document.querySelector(location.hash)
    if (element) {
      smoothScrollIntoViewIfNeeded(element, {
        behavior: 'smooth',
        block: 'start',
        inline: 'end',
      })
    }
  }
  return children
}

ScrollIntoView.propTypes = {
  children: propTypes.node.isRequired,
}

export default ScrollIntoView
