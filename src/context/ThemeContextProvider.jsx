import React, { useState, useEffect, createContext } from 'react'
import propTypes from 'prop-types'
import { THEME } from '../constants/theme'
export const ThemeContext = createContext()

const ThemeContextProvider = ({ children }) => {
  const [themeName, setThemeName] = useState('')
  const [hasRandomTheme, setHasRandomTheme] = useState(false)

  const randomTheme = () => {
    var keys = Object.keys(THEME)
    return THEME[keys[(keys.length * Math.random()) << 0]]
  }

  // Sync PersistedThemeName to app State
  useEffect(() => {
    const persistedThemeName = JSON.parse(localStorage.getItem('persistedThemeName'))

    if (persistedThemeName) {
      setThemeName(persistedThemeName)
    } else {
      setHasRandomTheme(true)
      setThemeName(randomTheme())
    }
  }, [])

  // If app themeName gets updated them persist it to state
  useEffect(() => {
    localStorage.setItem('persistedThemeName', JSON.stringify(themeName))
  }, [themeName])

  return (
    <ThemeContext.Provider value={{ setThemeName, themeName, hasRandomTheme, setHasRandomTheme }}>
      {children}
    </ThemeContext.Provider>
  )
}

ThemeContextProvider.propTypes = {
  children: propTypes.node.isRequired,
}

export default ThemeContextProvider
