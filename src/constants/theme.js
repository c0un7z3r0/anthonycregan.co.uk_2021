export const THEME = {
  ARTDECO: 'art-deco',
  ORIGINAL: 'original',
  SANTACRUZ: 'santa-cruz',
  SPRAWL: 'sprawl',
  HAAS: 'haas-music',
}
