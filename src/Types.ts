
import { SVGiconListTypes } from './components/common/svgIcon/svgIcon'

export type FixMeLater = any

export type ThemeNamesListType = 
  'art-deco' |
  'original' |
  'santa-cruz' |
  'sprawl' |
  'haas-music'

export type ThemeNameType = {
  themeName: ThemeNamesListType
}

export type ChildrenType = {
  children: JSX.Element | JSX.Element[]
}

export type SubMenuItemPropsType = {
  hash: string,
  label: string
}
export type NavMenuItemType = {
  label: string,
  themeName?: ThemeNamesListType,
  path: string,
  icon?: SVGiconListTypes,
  subMenu?: SubMenuItemPropsType[]
}