import React, { useContext, lazy, Suspense } from 'react'
import { ThemeContext } from './context/ThemeContextProvider'
import { Routes, Route } from 'react-router-dom'
import PageLoadingSpinner from './components/common/pageLoadingSpinner/pageLoadingSpinner'

const RootView = lazy(() => import(/* webpackChunkName: "RootView"*/ './components/views/rootView'))
const Home = lazy(() => import(/* webpackChunkName: "Home"*/ './components/content/home/home'))
const Portfolio = lazy(() =>
  import(/* webpackChunkName: "Portfolio"*/ './components/content/portfolio/portfolio')
)
const Resume = lazy(() =>
  import(/* webpackChunkName: "Resume"*/ './components/content/resume/resume')
)
const Projects = lazy(() =>
  import(/* webpackChunkName: "Projects"*/ './components/content/projects/projects')
)
const Contact = lazy(() =>
  import(/* webpackChunkName: "Contact"*/ './components/content/contact/contact')
)
const FourOhFour = lazy(() =>
  import(/* webpackChunkName: "FourOhFour"*/ './components/content/fourOhFour/fourOhFour')
)

import './public/css/normalize.css'
import './public/css/scrollbars.scss'
import './public/css/typography.scss'
import './public/css/global.scss'
import useGoogleAnalytics from './hooks/useGoogleAnalytics'

const App = () : JSX.Element => {
  const { themeName, hasRandomTheme, setHasRandomTheme } = useContext(ThemeContext)
  useGoogleAnalytics('G-SW44D8VC0N')

  return (
    <Suspense fallback={<PageLoadingSpinner />}>
      <RootView
        themeName={themeName}
        hasRandomTheme={hasRandomTheme}
        setHasRandomTheme={setHasRandomTheme}
      >
        <Suspense fallback={<PageLoadingSpinner />}>
          <Routes>
            <Route path="/" element={<Home themeName={themeName} />} />
            <Route path="/portfolio" element={<Portfolio themeName={themeName} />} />
            <Route path="/resume" element={<Resume themeName={themeName} />} />
            <Route path="/projects" element={<Projects themeName={themeName} />} />
            <Route path="/contact" element={<Contact themeName={themeName} />} />
            <Route path="*" element={<FourOhFour themeName={themeName} />} />
          </Routes>
        </Suspense>
      </RootView>
    </Suspense>
  )
}

export default App
